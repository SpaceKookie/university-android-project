package androidlab.exercise7_1.utility;

/* 
 * Copyright (c) 2013 Katharina Sabel
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * This is basically just a compilation of integer flags that will indicate what part of the UI needs to be rendered
 * 
 * @author Katharina
 * 
 */
public class RenderStatus {

  public static final int RENDER_EVERYTHING = 0x00000000;
  public static final int LOGIN_SCREEN_TEXTVIEW = 0x00000001;
  public static final int OVERVIEW_APP_STATUS_ENABLED = 0x00000002;
  public static final int OVERVIEW_APP_STATUS_DISABLED = 0x00000003;
  public static final int OVERVIEW_SETTINGS = 0x00000004;
  public static final int REFRESH_ACTIVITY_ICON = 0x00000005;

  public RenderStatus() {
	// TODO: Bake some cookies
  }

}
