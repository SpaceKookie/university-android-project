package androidlab.exercise3_1;

import android.support.v4.app.DialogFragment;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import java.util.Calendar;

public class DatePicker extends DialogFragment implements DatePickerDialog.OnDateSetListener {

  SetDataFragment sdf = new SetDataFragment();

  public Dialog onCreateDialog(Bundle savedInstanceState) {

	final Calendar cal = Calendar.getInstance();
	int day = cal.get(Calendar.DAY_OF_MONTH);
	int month = cal.get(Calendar.MONTH);
	int year = cal.get(Calendar.YEAR);

	return new DatePickerDialog(getActivity(), this, year, month, day);
  }

  @Override
  public void onDateSet(android.widget.DatePicker view, int year, int monthOfYear, int dayOfMonth) {

	String zero = "0";
	String newYear = Integer.toString(year);
	String month = Integer.toString(OmniTools.fixCal(monthOfYear));
	String day = Integer.toString(dayOfMonth);

	if (monthOfYear < 10) {
	  month = zero.concat(month);

	}
	if (dayOfMonth < 10) {
	  day = zero.concat(day);

	}

	Intent dateTravel = new Intent("LowOrbitIonCannon");
	dateTravel.putExtra("type", "date");
	dateTravel.putExtra("year", newYear);
	dateTravel.putExtra("month", month);
	dateTravel.putExtra("day", day);

	getActivity().sendBroadcast(dateTravel);

  }
}
