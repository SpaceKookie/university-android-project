package androidlab.exercise3_1.notification;

import java.util.ArrayList;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Intent;
import android.location.Location;
import androidlab.exercise3_1.MainActivity;
import androidlab.exercise3_1.adapter.DatabaseAdapter;
import androidlab.exercise3_1.model.ToDoItem;

/**
 * <h2>Service that handles the creation of alarm which generate a notification</h2>
 * This service is called every time the items in the database changes, more
 * precisely when the {@link MainActivity} recognizes that a item was created,
 * edited or deleted. Also when all items in the database were deleted this
 * service will be invoked. The service itself creates for every ToDo item for
 * which the application should remind an alarm which finally generate a
 * notification. So that alarms which are set also can be deleted, this service
 * stores the {@link PendingIntent} of an alarm in a list
 * {@link RemindService#alarmIntents}.
 * 
 * @see {@link IntentService}, {@link StartRemindService},
 *      {@link DatabaseAdapter}, {@link MainActivity}, {@link AlarmManager}
 * @author Seminar 'Introduction to Android Smartphone Programming', University
 *         of Freiburg
 * @version 1.3
 */
public class RemindService extends IntentService {

    /** Name for the worker thread of the service. */
    public static final String REMIND_SERVICE = "androidlab.exercise3_1.notification.remind_service";
    /**
     * Intent action of the intents which are broadcasted to the
     * {@link DisplayNotificationReceiver} receiver.
     */
    private static final String ALARM = "androidlab.exercise3_1.notification.remind_alarm_";
    /**
     * Intent action of the intents which indicates that the ToDo items in the
     * database were changed and which are broadcasted to this service.
     */
    public static String UPDATE_REMIND = "androidlab.exercise3_1.notification.update_remind";
    /**
     * Intent action of the intents which indicates the ALL alarms should be
     * canceled and which are broadcasted to this service.
     */
    public static String DELETE_REMIND = "androidlab.exercise3_1.notification.del_remind";
    /**
     * Storage of {@link PendingIntent}s which are set as a alarm to the
     * {@link AlarmManager}. With these intents the service can cancel the
     * alarms if needed.
     **/
    private static ArrayList<PendingIntent> alarmIntents = new ArrayList<PendingIntent>();

    /**
     * Empty constructor which calls the super class with name
     * {@link RemindService#REMIND_SERVICE}.
     **/
    public RemindService() {
	super(REMIND_SERVICE);
    }

    /**
     * Invoked when the service was started. Depending on the given intent
     * action the method will firstly call
     * {@link RemindService#initializeNotification()} for deleting all alarms
     * which were set and to create new alarms for all upcoming ToDo items or
     * secondly it will only delete all alarms by calling the method
     * {@link RemindService#deleteAllAlarms()}. The first case is when the
     * intent action informs about the completion of the boot process (
     * {@link Intent#ACTION_BOOT_COMPLETED}) or about the update of the dataset
     * ({@link RemindService#UPDATE_REMIND}). The latter case is only when all
     * items are removed from the database.
     * 
     * @param intent
     *            The value passed to startService(Intent).
     * @see {@link IntentService#onHandleIntent(Intent)},
     *      {@link RemindService#UPDATE_REMIND},
     *      {@link RemindService#DELETE_REMIND},
     *      {@link Intent#ACTION_BOOT_COMPLETED}
     **/
    @Override
    protected void onHandleIntent(Intent intent) {
	if (intent == null)
	    return;
	if (intent.getAction() == null)
	    return;
	if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
	    initializeNotification();
	} else if (intent.getAction().equals(UPDATE_REMIND)) {
	    initializeNotification();
	} else if (intent.getAction().equals(DELETE_REMIND)) {
	    deleteAllAlarms();
	}
    }

    /**
     * Method which initiate the deletion of all enabled alarms by calling
     * {@link RemindService#deleteAllAlarms()}, which fetches all ToDo items for
     * which the service should notify (see
     * {@link DatabaseAdapter#getUpcomingToDoItems(android.content.Context)})
     * and creates for each item an alarm (
     * {@link RemindService#createAlarm(int, String, String, long)}).
     **/
    private void initializeNotification() {
	deleteAllAlarms();
	ArrayList<ToDoItem> upcomingItems = DatabaseAdapter
		.getUpcomingToDoItems(this);
	for (ToDoItem todo : upcomingItems) {
	    createAlarm(todo.getId(), todo.getTitle(), todo.getDescription(),
		    todo.getRemindTimeInMillis());
	}
    }

    /**
     * Creates an alarm for the occurrence of a remind date of a ToDo item. The
     * needed information are given by the parameters. The method creates a
     * {@link PendingIntent} which contains an intent including all the needed
     * information about the ToDo item. This will be set as alarm to the
     * {@link AlarmManager}. Such an {@link PendingIntent} is also stored in the
     * list {@link RemindService#alarmIntents} so that the alarm also can be
     * canceled in the future.
     * 
     * @param id
     *            Identifier of the ToDo item for which the application should
     *            remind.
     * @param title
     *            Title of the ToDo item for which the application should
     *            remind.
     * @param description
     *            Description of the ToDo item for which the application should
     *            remind.
     * @param date
     *            Date when the application should remind as milliseconds from
     *            Jan. 1, 1970 to the remind date.
     **/
    private void createAlarm(int id, String title, String description, long date) {
	AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
	Intent intent = new Intent(this, DisplayNotificationReceiver.class);
	intent.setAction(ALARM + String.valueOf(id));
	intent.putExtra(ToDoItem.ID, id);
	intent.putExtra(ToDoItem.TITLE, title);
	intent.putExtra(ToDoItem.DESCRIPTION, description);
	PendingIntent contentIntent = PendingIntent.getBroadcast(this, id,
		intent, PendingIntent.FLAG_UPDATE_CURRENT);
	alarmIntents.add(contentIntent);
	alarmManager.set(AlarmManager.RTC, date, contentIntent);
    }

    /**
     * Cancels all set alarms from the {@link AlarmManager} by calling
     * {@link RemindService#deleteAlarm(PendingIntent)} for each stored
     * {@link PendingIntent} in the list {@link RemindService#alarmIntents}.
     **/
    private void deleteAllAlarms() {
	for (PendingIntent intent : alarmIntents) {
	    deleteAlarm(intent);
	}
	alarmIntents.clear();
    }

    /**
     * Removes a single alarm (that corresponds to the given
     * {@link PendingIntent}) from the {@link AlarmManager}.
     * 
     * @param pendingIntent
     *            IntentSender for which the alarm should be remove from the
     *            {@link AlarmManager}.
     **/
    private void deleteAlarm(PendingIntent pendingIntent) {
	AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
	alarmManager.cancel(pendingIntent);
    }

}
