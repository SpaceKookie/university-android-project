package androidlab.exercise3_1.adapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import samples.Samples;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidlab.exercise3_1.*;
import androidlab.exercise3_1.model.*;
import androidlab.exercise3_1.notification.*;
import androidlab.exercise3_1.widget.*;

/**
 * <h2>Class providing the database access</h2> This class provides all the functionality for accessing the database. Because there are
 * multiple processes which will access at the same time to these functions, the class provides only static methods so that the opening and
 * closing of the database itself is done within these methods (and so there will no exceptions because of accessing to a closed database).
 * To create a customized database the class contains also the file name {@link DatabaseAdapter#DB_FILENAME}, the version
 * {@link DatabaseAdapter#DB_VERSION} of this own database as well as the table name {@link DatabaseAdapter#TABLE_NAME}. The creation,
 * access, update and deletion of this database is done by the nested class {@link DatabaseHelper}.
 * 
 * @see {@link SQLiteOpenHelper}, {@link DatabaseHelper}, {@link ToDoItem}, {@link MainActivity}, {@link RemindService}, {@link Widget},
 *      {@link WidgetUpdate}
 * @author Seminar 'Introduction to Android Smartphone Programming', University of Freiburg
 * @version 1.3
 **/
public class DatabaseAdapter {

  /** Name of the database file (needed for the {@link DatabaseHelper}). */
  private static final String DB_FILENAME = "todoDB";
  /** Version of the database (needed for the {@link DatabaseHelper}) */
  private static final int DB_VERSION = 4;
  /** Name of the database table which contains the ToDo items */
  private static final String TABLE_NAME = "todos";

  /**
   * Nested class which extends from the {@link SQLiteOpenHelper} and which provides all basic functions to create, open, close and delete
   * the database as well as it also provides functions to query for database entries.
   **/
  public static class DatabaseHelper extends SQLiteOpenHelper {

	/**
	 * Constructor of the own database helper which will provide all the basic functions in a given context.
	 * 
	 * @param context
	 *          The Context in which the database helper is created.
	 */
	public DatabaseHelper(Context context) {

	  super(context, DB_FILENAME, null, DB_VERSION);
	}

	/**
	 * Method will be called if the database does not exists and some process tries to access to the database for the first time. The method
	 * itself creates a table where the ToDo items can be stored. Therefore this table contains the same properties as a {@link ToDoItem}
	 * and the name of these columns are given by the property identifier (@link {@link ToDoItem#ID}, {@link ToDoItem#TITLE},
	 * {@link ToDoItem#DESCRIPTION}, {@link ToDoItem#DONE}, {@link ToDoItem#REMIND}, {@link ToDoItem#DATE} ). The used datatypes are
	 * straight forward excepted the remind and the done status (will be converted to 1 if {@code true}, 0 otherwise) as well as the date.
	 * Because SQLite only can handle simple datatypes as {@code String} or {@code int}, the {@code long} must be converted to a
	 * {@code String} ({@code int} will not work, because the range of {@code int} does not fit to the milliseconds from Jan. 1, 1971).
	 * Consider that working with this date column does not react as expected (e.g. comparison).
	 * 
	 * @param db
	 *          The database.
	 * @see {@link SQLiteOpenHelper#onCreate(SQLiteDatabase)}
	 **/
	@Override
	public void onCreate(SQLiteDatabase db) {

	  db.execSQL(String.format(
		  "CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, %s TEXT, %s TEXT, %s INTEGER NOT NULL, %s INTEGER NOT NULL, %s TEXT);",
		  TABLE_NAME, ToDoItem.ID, ToDoItem.TITLE, ToDoItem.DESCRIPTION, ToDoItem.REMIND, ToDoItem.DONE, ToDoItem.DATE, ToDoItem.GPSX,
		  ToDoItem.GPSY));
	}

	/**
	 * Called when the database needs to be upgraded. Because the database stays the same at the moment there is no need for a concrete
	 * implementation.
	 * 
	 * @param db
	 *          The database.
	 * @param oldVersion
	 *          The old database version.
	 * @param newVersion
	 *          The new database version.
	 * @see {@link SQLiteOpenHelper#onUpgrade(SQLiteDatabase, int, int)}
	 **/
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

	  // Currently not needed
	}
  }

  /**
   * Method will instantiate a {@link DatabaseHelper} and opens a readable database. In the table of the ToDo items
   * {@link DatabaseAdapter#TABLE_NAME} the entry for which the application should remind next will be fetched. Through a {@link Cursor} the
   * values of the entry will be accessed, if this contains more than one row. If there is no row the method will return {@code null},
   * otherwise with the received values a ToDo item will be instantiated and returned. Before returning the result object, the
   * {@link DatabaseHelper} will be closed.
   * 
   * @param context
   *          The Context in which this access to the database is invoked.
   * @return The ToDo item for which the application should remind if such an entry exists in the database, this means that the ToDo item is
   *         not done, the application should remind for this item and the remind date is after the current date. If there exists no such
   *         item the method will return {@code null}.
   * 
   * @see {@link ToDoItem}, {@link SQLiteDatabase}, {@link DatabaseHelper#getReadableDatabase()}
   */
  public synchronized static ToDoItem getUpcomingToDoItem(Context context) {

	DatabaseHelper helper = new DatabaseHelper(context);
	SQLiteDatabase db = helper.getReadableDatabase();
	String where = ToDoItem.REMIND + " = 1 AND " + ToDoItem.DONE + " = 0";
	Cursor cursor = db.query(TABLE_NAME, new String[] { ToDoItem.ID, ToDoItem.TITLE, ToDoItem.DESCRIPTION, ToDoItem.DONE, ToDoItem.REMIND,
		ToDoItem.DATE }, where, null, null, null, null);
	ToDoItem todo = null;
	if (cursor.getCount() > 0) {
	  while (cursor.moveToNext()) {
		try {
		  ToDoItem temp = new ToDoItem(cursor.getInt(cursor.getColumnIndexOrThrow(ToDoItem.ID)), cursor.getString(cursor
			  .getColumnIndexOrThrow(ToDoItem.TITLE)), cursor.getString(cursor.getColumnIndexOrThrow(ToDoItem.DESCRIPTION)),
			  cursor.getInt(cursor.getColumnIndexOrThrow(ToDoItem.REMIND)) > 0,
			  cursor.getInt(cursor.getColumnIndexOrThrow(ToDoItem.DONE)) > 0, Long.valueOf(cursor.getString(cursor
				  .getColumnIndexOrThrow(ToDoItem.DATE))));
		  if (todo == null) {
			if (temp.isAfterNow())
			  todo = temp;
		  }
		  else {
			if (temp.isAfterNow() && todo.getDate().after(temp.getDate()))
			  todo = temp;
		  }
		}
		catch (Exception e) {
		  cursor.close();
		  db.close();
		  return todo;
		}
	  }
	}
	cursor.close();
	db.close();
	return todo;
  }

  /**
   * Method will instantiate a {@link DatabaseHelper} and opens a readable database. In the table of the ToDo items
   * {@link DatabaseAdapter#TABLE_NAME} all entries for which the application should remind will be fetched and through a {@link Cursor} the
   * values of each row will be accessed. For such an row a new Instance of {@link ToDoItem} is created with the corresponding values. All
   * created items will be added to a list which the method returns. Before returning the result object, the {@link DatabaseHelper} will be
   * closed.
   * 
   * @param context
   *          The Context in which this access to the database is invoked.
   * @return List of all ToDo items which are stored in the database and which are enable for reminding. This means that the ToDo items are
   *         not done, the application should remind for these items and the remind date is after the current date.
   * 
   * @see {@link ToDoItem}, {@link SQLiteDatabase}, {@link DatabaseHelper#getReadableDatabase()}
   */
  public synchronized static ArrayList<ToDoItem> getUpcomingToDoItems(Context context) {

	DatabaseHelper helper = new DatabaseHelper(context);
	SQLiteDatabase db = helper.getReadableDatabase();
	String where = ToDoItem.REMIND + " = 1 AND " + ToDoItem.DONE + " = 0";
	Cursor cursor = db.query(TABLE_NAME, new String[] { ToDoItem.ID, ToDoItem.TITLE, ToDoItem.DESCRIPTION, ToDoItem.DONE, ToDoItem.REMIND,
		ToDoItem.DATE }, where, null, null, null, null);
	ArrayList<ToDoItem> todos = new ArrayList<ToDoItem>();
	if (cursor.getCount() > 0) {
	  while (cursor.moveToNext()) {
		try {
		  ToDoItem todo = new ToDoItem(cursor.getInt(cursor.getColumnIndexOrThrow(ToDoItem.ID)), cursor.getString(cursor
			  .getColumnIndexOrThrow(ToDoItem.TITLE)), cursor.getString(cursor.getColumnIndexOrThrow(ToDoItem.DESCRIPTION)),
			  cursor.getInt(cursor.getColumnIndexOrThrow(ToDoItem.REMIND)) > 0,
			  cursor.getInt(cursor.getColumnIndexOrThrow(ToDoItem.DONE)) > 0, Long.valueOf(cursor.getString(cursor
				  .getColumnIndexOrThrow(ToDoItem.DATE))));
		  if (todo.isAfterNow())
			todos.add(todo);
		}
		catch (Exception e) {
		  cursor.close();
		  db.close();
		  return todos;
		}
	  }
	}
	cursor.close();
	db.close();
	return todos;
  }

  /**
   * Method will instantiate a {@link DatabaseHelper} and opens a readable database. In the table of the ToDo items
   * {@link DatabaseAdapter#TABLE_NAME} all entries will be fetched and through a {@link Cursor} the values of each row will be accessed.
   * For such an row a new Instance of {@link ToDoItem} is created with the corresponding values. All created items will be added to a list
   * which the method returns. Before returning the result object, the {@link DatabaseHelper} will be closed.
   * 
   * @param context
   *          The Context in which this access to the database is invoked.
   * @return List of all ToDo items which are stored in the database.
   * 
   * @see {@link ToDoItem}, {@link SQLiteDatabase}, {@link DatabaseHelper#getReadableDatabase()}
   */
  public synchronized static ArrayList<ToDoItem> fetchAllToDoItems(Context context) {

	DatabaseHelper helper = new DatabaseHelper(context);
	SQLiteDatabase db = helper.getReadableDatabase();
	String[] fields = new String[] { ToDoItem.ID, ToDoItem.TITLE, ToDoItem.DESCRIPTION, ToDoItem.REMIND, ToDoItem.DONE, ToDoItem.DATE };
	Cursor cursor = db.query(TABLE_NAME, fields, null, null, null, null, null);
	ArrayList<ToDoItem> todos = new ArrayList<ToDoItem>();
	Comparator<ToDoItem> comp = new ToDoComparator();
	if (cursor.getCount() > 0) {
	  while (cursor.moveToNext()) {
		try {
		  ToDoItem todo = new ToDoItem(cursor.getInt(cursor.getColumnIndexOrThrow(ToDoItem.ID)), cursor.getString(cursor
			  .getColumnIndexOrThrow(ToDoItem.TITLE)), cursor.getString(cursor.getColumnIndexOrThrow(ToDoItem.DESCRIPTION)),
			  cursor.getInt(cursor.getColumnIndexOrThrow(ToDoItem.REMIND)) > 0,
			  cursor.getInt(cursor.getColumnIndexOrThrow(ToDoItem.DONE)) > 0, Long.valueOf(cursor.getString(cursor
				  .getColumnIndexOrThrow(ToDoItem.DATE))));
		  todos.add(todo);
		}
		catch (Exception e) {
		  cursor.close();
		  db.close();
		  Collections.sort(todos, comp);
		  return todos;
		}
	  }
	}
	cursor.close();
	db.close();
	Collections.sort(todos, comp);
	return todos;
  }

  /**
   * Method will instantiate a {@link DatabaseHelper} and opens a readable database. In the table of the ToDo items
   * {@link DatabaseAdapter#TABLE_NAME} the entry which has the given identifier will be fetched. Through a {@link Cursor} the values of the
   * entry will be accessed, if this contains more than one row. If there is no row the method will return {@code null}, otherwise with the
   * received values a ToDo item will be instantiated and returned. Before returning the result object, the {@link DatabaseHelper} will be
   * closed.
   * 
   * @param id
   *          Identifier of the ToDo item which should be fetched.
   * @param context
   *          The Context in which this access to the database is invoked.
   * @return The ToDo item which has the given identifier if such an entry exists in the database, otherwise null.
   * 
   * @see {@link ToDoItem}, {@link SQLiteDatabase}, {@link DatabaseHelper#getReadableDatabase()}
   */
  public synchronized static ToDoItem fetchToDoItemWith(int id, Context context) {

	DatabaseHelper helper = new DatabaseHelper(context);
	SQLiteDatabase db = helper.getReadableDatabase();
	ToDoItem result = null;
	Cursor cursor = db.query(TABLE_NAME, new String[] { ToDoItem.ID, ToDoItem.TITLE, ToDoItem.DESCRIPTION, ToDoItem.DONE, ToDoItem.REMIND,
		ToDoItem.DATE }, ToDoItem.ID + "=" + id, null, null, null, null);
	if (cursor.getCount() > 0) {
	  cursor.moveToFirst();
	  try {
		result = new ToDoItem(cursor.getInt(cursor.getColumnIndexOrThrow(ToDoItem.ID)), cursor.getString(cursor
			.getColumnIndexOrThrow(ToDoItem.TITLE)), cursor.getString(cursor.getColumnIndexOrThrow(ToDoItem.DESCRIPTION)),
			cursor.getInt(cursor.getColumnIndexOrThrow(ToDoItem.REMIND)) > 0,
			cursor.getInt(cursor.getColumnIndexOrThrow(ToDoItem.DONE)) > 0, Long.valueOf(cursor.getString(cursor
				.getColumnIndexOrThrow(ToDoItem.DATE))));
	  }
	  catch (Exception e) {
		cursor.close();
		db.close();
		return result;
	  }
	}
	cursor.close();
	db.close();
	return result;
  }

  /**
   * Method will instantiate a {@link DatabaseHelper} and opens a writable database. In the table of the ToDo items
   * {@link DatabaseAdapter#TABLE_NAME} a new entry will be added which has the given corresponding values. After this request the
   * {@link DatabaseHelper} will be closed. If this query was successful the method will return the identifier of the new inserted item.
   * (For the conversion of the {@code boolean} and the {@code long} value see {@link DatabaseHelper#onCreate(SQLiteDatabase)}.)
   * 
   * @param title
   *          Title of the new ToDo item.
   * @param description
   *          Description of the new ToDo item.
   * @param done
   *          Done status of the new ToDo item.
   * @param remind
   *          Remind status of the new ToDo item.
   * @param date
   *          The remind date of the new ToDo item.
   * @param gpsX
   * 		  The X-Axis of the saved position         
   * @param gpsY
   * 		  The Y-Axis of the saved position
   * @param context
   *          The Context in which this access to the database is invoked.
   * @return The identifier of the new inserted item if the request was successful, otherwise -1.
   * 
   * @see {@link ToDoItem}, {@link SQLiteDatabase}, {@link DatabaseHelper#getWritableDatabase()}
   */
  public synchronized static int createToDoItem(String title, String description, boolean done, boolean remind, long date, /**
   * long gpsX, long
   * gpsY,
   */
  Context context) {

	DatabaseHelper helper = new DatabaseHelper(context);
	SQLiteDatabase db = helper.getWritableDatabase();
	ContentValues values = new ContentValues();
	values.put(ToDoItem.TITLE, title);
	values.put(ToDoItem.DESCRIPTION, description);
	values.put(ToDoItem.DONE, done ? 1 : 0);
	values.put(ToDoItem.REMIND, remind ? 1 : 0);
	values.put(ToDoItem.DATE, String.valueOf(date));
	// values.put(ToDoItem.GPSX, gpsX);
	// values.put(ToDoItem.GPSY, gpsY);
	int result = (int) db.insert(TABLE_NAME, null, values);
	db.close();
	return result;
  }

  /**
   * Method will instantiate a {@link DatabaseHelper} and opens a writable database. In the table of the ToDo items
   * {@link DatabaseAdapter#TABLE_NAME} the entry which has the given identifier will be updated. All columns except the identifier column
   * were set to the corresponding given value. After this request the {@link DatabaseHelper} will be closed. If this query was successful
   * the method will return {@code true}, otherwise {@code false}. (For the conversion of the {@code boolean} and the {@code long} value see
   * {@link DatabaseHelper#onCreate(SQLiteDatabase)}.)
   * 
   * @param id
   *          Identifier of the item which should be updated.
   * @param title
   *          New title of this ToDo item.
   * @param description
   *          New description of the ToDo item.
   * @param done
   *          New done status of the ToDo item.
   * @param remind
   *          New remind status of the ToDo item.
   * @param date
   *          New remind date of the ToDo item.
   * @param context
   *          The Context in which this access to the database is invoked.
   * @return {@code true} if the item with the given identifier in the ToDo item table was updated, if not {@code false}.
   * 
   * @see {@link ToDoItem}, {@link SQLiteDatabase}, {@link DatabaseHelper#getWritableDatabase()}
   */
  public synchronized static boolean updateToDoItemWith(int id, String title, String description, boolean done, boolean remind, long date,
	  Context context) {

	DatabaseHelper helper = new DatabaseHelper(context);
	SQLiteDatabase db = helper.getWritableDatabase();
	ContentValues values = new ContentValues();
	values.put(ToDoItem.TITLE, title);
	values.put(ToDoItem.DESCRIPTION, description);
	values.put(ToDoItem.DONE, done ? 1 : 0);
	values.put(ToDoItem.REMIND, remind ? 1 : 0);
	values.put(ToDoItem.DATE, String.valueOf(date));
	boolean result = db.update(TABLE_NAME, values, ToDoItem.ID + '=' + id, null) > 0;
	db.close();
	return result;
  }

  /**
   * Method will instantiate a {@link DatabaseHelper} and opens a writable database. In the table of the ToDo items
   * {@link DatabaseAdapter#TABLE_NAME} the entry which has the given identifier will be updated. But only the done status of this entry
   * will set to the given {@code boolean} value. After this request the {@link DatabaseHelper} will be closed. If this query was successful
   * the method will return {@code true}, otherwise {@code false}. (For the conversion of the {@code boolean} and the {@code long} value see
   * {@link DatabaseHelper#onCreate(SQLiteDatabase)}.)
   * 
   * @param id
   *          Identifier of the item which should be updated.
   * @param done
   *          New done status of the ToDo item.
   * @param context
   *          The Context in which this access to the database is invoked.
   * @return {@code true} if the done status of item with the given identifier in the ToDo item table was updated, if not {@code false}.
   * 
   * @see {@link ToDoItem}, {@link SQLiteDatabase}, {@link DatabaseHelper#getWritableDatabase()}
   */
  public synchronized static boolean updateToDoItemWith(int id, boolean done, Context context) {

	DatabaseHelper helper = new DatabaseHelper(context);
	SQLiteDatabase db = helper.getWritableDatabase();
	ContentValues values = new ContentValues();
	values.put(ToDoItem.DONE, done ? 1 : 0);
	boolean result = db.update(TABLE_NAME, values, ToDoItem.ID + '=' + id, null) > 0;
	db.close();
	return result;
  }

  /**
   * Method will instantiate a {@link DatabaseHelper} and opens a writable database. In the table of the ToDo items
   * {@link DatabaseAdapter#TABLE_NAME} the entry which has the given identifier will be deleted and after this request the
   * {@link DatabaseHelper} will be closed. If this query was successful the method will return {@code true}, otherwise {@code false}.
   * 
   * @param id
   *          Identifier of the item which should be deleted.
   * @param context
   *          The Context in which this access to the database is invoked.
   * @return {@code true} if the item with the given identifier in the ToDo item table was deleted, if not {@code false}.
   * 
   * @see {@link ToDoItem}, {@link SQLiteDatabase}, {@link DatabaseHelper#getWritableDatabase()}
   */
  public synchronized static boolean deleteToDoItemWith(int id, Context context) {

	DatabaseHelper helper = new DatabaseHelper(context);
	SQLiteDatabase db = helper.getWritableDatabase();
	boolean result = db.delete(TABLE_NAME, ToDoItem.ID + "=" + id, null) > 0;
	db.close();
	return result;
  }

  /**
   * Method will instantiates a {@link DatabaseHelper} and opens a writable database. In the table of the ToDo items
   * {@link DatabaseAdapter#TABLE_NAME} will all entries be deleted and after this request the {@link DatabaseHelper} will be closed. If
   * this query was successful the method will return {@code true}, otherwise {@code false}.
   * 
   * @param context
   *          The Context in which this access to the database is invoked.
   * @return {@code true} if the items in the ToDo item table were deleted, if not {@code false}. (The {@code boolean} result value does not
   *         indicate that really all items where deleted, but the normal case is that all items were deleted.)
   * 
   * @see {@link ToDoItem}, {@link SQLiteDatabase}, {@link DatabaseHelper#getWritableDatabase()}
   */
  public synchronized static boolean deleteAllToDoItems(Context context) {

	DatabaseHelper helper = new DatabaseHelper(context);
	SQLiteDatabase db = helper.getWritableDatabase();
	boolean result = db.delete(TABLE_NAME, "1", null) >= 0;
	db.close();
	return result;
  }

  /**
   * Method will create a list of random sample ToDo items by calling the method {@link Samples#generateListOfToDoItems(Context)} and insert
   * all these items into the database ( {@link DatabaseAdapter#createToDoItem(String, String, boolean, boolean, long, Context)} ).
   * 
   * @param context
   *          The Context in which this access to the database is invoked.
   * @see {@link Samples}, {@link ToDoItem}
   */
  public synchronized static void insertTestdata(Context context) {

	ArrayList<ToDoItem> todos = Samples.generateListOfToDoItems(context);
	for (ToDoItem todo : todos) {
	  createToDoItem(todo.getTitle(), todo.getDescription(), todo.isDone(), todo.shouldRemind(), todo.getRemindTimeInMillis(), context);
	}
  }

}
