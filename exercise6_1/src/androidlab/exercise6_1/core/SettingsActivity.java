package androidlab.exercise6_1.core;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import androidlab.exercise6_1.gui.SettingsFragment;

/**
 * Only creates a fragment Transition to display the Settings Fragment because most of the Activity methods are being deprecated
 * 
 * @author Konstantin
 * 
 */
public class SettingsActivity extends PreferenceActivity {

	FragmentManager man;
	FragmentTransaction trans;

	@Override
	protected void onCreate(Bundle states) {

		super.onCreate(states);

		man = getFragmentManager();
		trans = man.beginTransaction();
		trans.replace(android.R.id.content, new SettingsFragment());
		trans.commit();
	}

	/**
	 * Calls the Phone Activity again to force the fragment to rebuild itself
	 */
	@Override
	public void onBackPressed() {

		super.onBackPressed();

	}
}