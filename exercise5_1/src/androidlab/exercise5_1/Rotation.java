package androidlab.exercise5_1;

/**
 * I wanted to use this object to capture the cubes current rotation to get a starting point for my end animation. Didn't quite work out
 * though, for some reason the cube would snap back into the start position (0,0,0) which displayed a six
 * 
 * @author Konstantin
 * 
 */

@Deprecated
public class Rotation {

  float x;
  float y;
  float z;

  public Rotation(float x, float y, float z) {

	this.x = x;
	this.y = y;
	this.z = z;
  }

  /**
   * Add float x to this.x
   * 
   * @param x
   */
  public void addX(float x) {

	this.x += x;
  }

  public void addY(float y) {

	this.y += y;
  }

  public void addZ(float z) {

	this.z += z;
  }

  /**
   * @return the x
   */
  public float getX() {

	return x;
  }

  /**
   * @param x
   *          the x to set
   */
  public void setX(float x) {

	this.x = x;
  }

  /**
   * @return the y
   */
  public float getY() {

	return y;
  }

  /**
   * @param y
   *          the y to set
   */
  public void setY(float y) {

	this.y = y;
  }

  /**
   * @return the z
   */
  public float getZ() {

	return z;
  }

  /**
   * @param z
   *          the z to set
   */
  public void setZ(float z) {

	this.z = z;
  }

}
