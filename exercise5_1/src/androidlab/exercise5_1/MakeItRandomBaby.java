package androidlab.exercise5_1;

import java.util.Random;

/**
 * 
 * I probably could have done this somewhere else but well what the heck. This class'es only function is to generate a random number
 * 
 * @author Konstantin
 * 
 */
public class MakeItRandomBaby {

  // Returns a random number to work with
  public static int random() {

	Random r = new Random();

	return r.nextInt(5) + 1;
  }
}
