package androidlab.exercise7_1.core;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import androidlab.exercise7_1.R;
import androidlab.exercise7_1.fragments.full.PanicFragment;

public class PanicActivity extends Activity {

	private PanicFragment panic;
	private FragmentManager man;
	private FragmentTransaction trans;

	@Override
	protected void onCreate(Bundle states) {
		states = null;
		super.onCreate(states);

		panic = new PanicFragment();

		makeUI();

	}

	private void makeUI() {
		man = getFragmentManager();
		trans = man.beginTransaction();
		trans.replace(R.id.master_container, panic, "PANIC FRAGMENT");
		trans.commit();
	}

	@Override
	protected void onPause() {
		onStop();
		onDestroy();
	}

	@Override
	protected void onResume() {

	}

}
