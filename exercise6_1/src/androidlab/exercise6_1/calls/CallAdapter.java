package androidlab.exercise6_1.calls;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import androidlab.exercise6_1.R;
import androidlab.exercise6_1.data.EvilMananger;

/**
 * Based on my brothers (Leander Sabel) CallAdapter. Please deduct points for this class as you see fit as I didn't concept it on my own and most of the writing
 * was based on my brothers source code (and my RemindersAdapter from exercise3_1 actually...) . Just wanted to let you know
 * 
 * @author Konstantin
 * 
 */
public class CallAdapter extends BaseAdapter {

	private ArrayList<Call> calls = new ArrayList<Call>();

	@Override
	public View getView(int position, View v, ViewGroup parent) {

		View row = v;
		LogHolder holder = null;

		// Create view if null
		if (row == null) {
			Context context = parent.getContext();
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row = inflater.inflate(R.layout.listview_item, parent, false);

			holder = new LogHolder();
			holder.position = position;

			holder.number = (TextView) row.findViewById(R.id.row_item_number);
			holder.time = (TextView) row.findViewById(R.id.row_item_date);
			holder.action = (TextView) row.findViewById(R.id.row_item_action);
			row.setTag(holder);

		} else {
			holder = (LogHolder) row.getTag();
		}

		final ArrayList<Call> calls = EvilMananger.getInstance().getDatabase().getCallInfo();

		// I actually wanted to change this to a button but I got nullpointers when trying to use v.getTag(). Not entirely sure why because I only assume that
		// Android would pass in a view with the onClick method and it is working the way it is now, accessing the row-view and using the onClickListener on it
		row.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				int position = ((LogHolder) v.getTag()).position;
				Call call = calls.get(position);

				Uri number = Uri.parse("tel:" + call.getNumber());
				Intent dial = new Intent(Intent.ACTION_DIAL);
				dial.setData(number);
				v.getContext().startActivity(dial);
			}
		});

		// Set the correct values
		holder.number.setText(calls.get(position).getNumber());
		holder.time.setText(calls.get(position).getDate());
		holder.action.setText(calls.get(position).getAction());

		return row;
	}

	/**
	 * @param calls
	 *            the calls to set
	 */
	public void setCalls(ArrayList<Call> calls) {

		this.calls = calls;
	}

	@Override
	public int getCount() {

		return calls.size();
	}

	@Override
	public Object getItem(int position) {

		return calls.get(position);
	}

	public static class LogHolder {

		public int position;
		public TextView number;
		public TextView time;
		public TextView action;
		public Button respond;
	}

	@Override
	public long getItemId(int position) {

		return position;
	}

}
