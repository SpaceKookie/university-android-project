package androidlab.exercise3_1.widget;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import androidlab.exercise3_1.*;
import androidlab.exercise3_1.adapter.*;
import androidlab.exercise3_1.model.*;

/**
 * <h2>Customized AppWigetProvider</h2>
 * 
 * This class implements the customized widget and handles all interactions
 * between the system and the widget(s). So it provides methods which are called
 * when a widget is instantiated, when the widget(s) should be updated and also
 * when all instances of this widget are deleted. Because the method
 * {@link Widget#onUpdate(Context, AppWidgetManager, int[])} is only called
 * minimal every 30 minutes which updates the interface of the widget, an
 * {@link AlarmManager} is used to update the widget more often. The widget
 * itself should show the next remind occurrence, means the ToDo item which
 * remind date occurs next. Also the widget shows the title of the application.
 * 
 * @see {@link ToDoItem}, {@link DatabaseAdapter}, {@link WidgetUpdate}
 * @author Seminar 'Introduction to Android Smartphone Programming', University
 *         of Freiburg
 * @version 1.3
 **/
public class Widget extends AppWidgetProvider {

	/**
	 * Called when the system braodcasts an
	 * {@link AppWidgetManager#ACTION_APPWIDGET_UPDATE} intent. For each
	 * existing widget the shown information in this widget will be updated.
	 * More precisely, the ToDo item which remind occurrence is the next will be
	 * fetched from the database by calling
	 * {@link DatabaseAdapter#getUpcomingToDoItem(Context)}. If there exist such
	 * an item its information are set to the text fields in the layout
	 * {@code R.layout.widget} otherwise the text field in the widget should
	 * show that there is no item.
	 * 
	 * @param context
	 *            The Context in which this receiver is running.
	 * @param appWidgetManager
	 *            A AppWidgetManager object you can call
	 *            updateAppWidget(ComponentName, RemoteViews) on.
	 * @param appWidgetIds
	 *            The appWidgetIds for which an update is needed. Note that this
	 *            may be all of the AppWidget instances for this provider, or
	 *            just a subset of them.
	 * 
	 * @see {@link AppWidgetProvider#onUpdate(Context, AppWidgetManager, int[])}
	 *      , {@link DatabaseAdapter}
	 **/
	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager,
			int[] appWidgetIds) {
		ComponentName thisWidget = new ComponentName(context, Widget.class);
		for (int widgetId : appWidgetManager.getAppWidgetIds(thisWidget)) {
			ToDoItem todo = DatabaseAdapter.getUpcomingToDoItem(context);
			RemoteViews updateViews = new RemoteViews(context.getPackageName(),
					R.layout.widget);
			updateViews.setTextViewText(R.id.widget_app,
					context.getString(R.string.app_name));
			Intent i = new Intent(context, MainActivity.class);
			PendingIntent pendingIntent = PendingIntent.getActivity(context, 0,
					i, PendingIntent.FLAG_UPDATE_CURRENT);
			updateViews.setOnClickPendingIntent(R.id.widget_app, pendingIntent);
			if (todo != null) {
				updateViews.setTextViewText(R.id.widget_title, todo.getTitle());
				updateViews.setTextViewText(R.id.widget_datetime,
						String.format("%1$tF %1$tR", todo.getDate().getTime()));
			} else {
				updateViews.setTextViewText(R.id.widget_title,
						context.getString(R.string.no_upcoming));
				updateViews.setTextViewText(R.id.widget_datetime, "");
			}
			appWidgetManager.updateAppWidget(widgetId, updateViews);
		}
	}

	/**
	 * Called when the last widget instance was deleted. Because of this there
	 * is not further reason for updating the widgets, so the Alarm which sends
	 * periodical intents to the {@link WidgetUpdate} BroadcastReceiver will be
	 * canceled. This alarm is created in the method
	 * {@link Widget#onEnabled(Context)}.
	 * 
	 * @see {@link AppWidgetProvider#onDisabled(Context)}, {@link AlarmManager}
	 **/
	@Override
	public void onDisabled(Context context) {
		Intent intent = new Intent(context, WidgetUpdate.class);
		PendingIntent sender = PendingIntent
				.getBroadcast(context, 0, intent, 0);
		AlarmManager alarmManager = (AlarmManager) context
				.getSystemService(Context.ALARM_SERVICE);
		alarmManager.cancel(sender);
		super.onDisabled(context);
	}

	/**
	 * Called when the AppWidget for this provider is instantiated. It will
	 * create an alarm which sends periodical intents to the
	 * {@link WidgetUpdate} BroadcastReceiver. This implementation allows to
	 * update the widget more often than only every half hour (
	 * {@link Widget#onUpdate(Context, AppWidgetManager, int[])} is only called
	 * every 30 minutes). The interval time of the alarm is stored in the
	 * resource {@code res/values/app_settings.xml} with the identifier
	 * {@code widget_updatetime}.
	 * 
	 * @see {@link AppWidgetProvider#onEnabled(Context)}, {@link AlarmManager}
	 **/
	@Override
	public void onEnabled(Context context) {
		super.onEnabled(context);
		AlarmManager alarmManager = (AlarmManager) context
				.getSystemService(Context.ALARM_SERVICE);
		Intent intent = new Intent(context, WidgetUpdate.class);
		intent.setAction(WidgetUpdate.RECEIVER);
		PendingIntent pi = PendingIntent.getBroadcast(context, 0, intent, 0);
		alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,
				System.currentTimeMillis(),
				context.getResources().getInteger(R.integer.widget_updatetime),
				pi);
	}
}
