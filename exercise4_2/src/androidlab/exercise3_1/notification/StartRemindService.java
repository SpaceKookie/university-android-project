package androidlab.exercise3_1.notification;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * <h2>Handles the instantiation of the remind alarms after system boot</h2>
 * This BroadcastReceiver will be called when the boot of the system is
 * complete. After that for all ToDo items for which the application should
 * remind must be created an alarm. Because of this the
 * {@link StartRemindService#onReceive(Context, Intent)} starts the
 * {@link RemindService}.
 * 
 * @see {@link BroadcastReceiver}, {@link RemindService}
 * @author Seminar 'Introduction to Android Smartphone Programming', University
 *         of Freiburg
 * @version 1.3
 **/
public class StartRemindService extends BroadcastReceiver {

	/**
	 * Called if this BraodcastReceiver receives the intent which informs about
	 * the completion of the boot process. It will start the
	 * {@link RemindService} so that the initial setup of the reminding can be
	 * done (see {@link RemindService#onHandleIntent(Intent)}).
	 * 
	 * @param context
	 *            The Context in which the receiver is running.
	 * @param intent
	 *            The Intent being received.
	 * 
	 * @see {@link BroadcastReceiver#onReceive(Context, Intent)},
	 *      {@link RemindService}
	 **/
	@Override
	public void onReceive(Context context, Intent intent) {
		intent.setClass(context, RemindService.class);
		context.startService(intent);
	}

}
