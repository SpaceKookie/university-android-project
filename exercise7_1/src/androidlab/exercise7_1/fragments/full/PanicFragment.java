package androidlab.exercise7_1.fragments.full;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidlab.exercise7_1.R;
import androidlab.exercise7_1.core.AlarmService;
import androidlab.exercise7_1.core.NumPadListener;
import androidlab.exercise7_1.utility.AppSettingsHelper;
import androidlab.exercise7_1.utility.RenderStatus;
import androidlab.exercise7_1.utility.Utility;

/* 
 * Copyright (c) 2013 Katharina Sabel
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * First Fragment to open on App launch. Will display a login screen. @InitialFragment might be called to setup the
 * application (set PIN etc.)
 * 
 * @author Katharina
 * 
 */
public class PanicFragment extends Fragment {

	// ******* GLOBAL VARIABLES *******
	private View view;
	private Context context;
	private IntentFilter filter;
	private OnClickListener numpad;
	private boolean registering;

	// ******* GUI ELEMENTS *******
	private Button one, two, three, four, five, six, seven, eight, nine, zero, submit, forgot, back;
	private EditText pinfield;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle states) {
		view = inflater.inflate(R.layout.layout_panic_screen, container, false);
		this.context = getActivity();

		// Setup for the broadcast receiver and onCLickListener
		filter = new IntentFilter(AppSettingsHelper.PANIC_INTENT_FILTER);
		numpad = new NumPadListener(context);

		context.registerReceiver(receiver, filter);
		registering = true;

		buildUI(RenderStatus.RENDER_EVERYTHING);
		return view;
	}

	/**
	 * Pausing the app, unregistering the broadcastreceiver and also resetting the onClickListener
	 */
	@Override
	public void onPause() {
		super.onPause();
		setOnClickListener(null);
		if (registering) {
			context.unregisterReceiver(receiver);
			registering = false;
		}
	}

	/**
	 * Causing the entire GUI to rebuild from scratch
	 */
	@Override
	public void onResume() {
		super.onResume();

		if (!registering) {
			context.registerReceiver(receiver, filter);
			registering = true;
		}

		buildUI(RenderStatus.RENDER_EVERYTHING);
	}

	private BroadcastReceiver receiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {

			Bundle extras = intent.getExtras();
			if (intent.hasExtra("ButtonID")) {
				int b = (Integer) extras.get("ButtonID");
				addToTextField(b);
			}

		}
	};

	/**
	 * Method to update the PIN Textfield and trigger the appripriate resonses
	 * 
	 * @param b
	 */
	private void addToTextField(Integer b) {

		// Clear the textfield
		// TODO: Only remove one char from the field?
		if (b == -1) {
			pinfield.setText(null);
		} else if (b == -2) {
			callLogin();
		} else {
			String temp = pinfield.getText().toString();

			// User @pin should only be 8 digits long
			if (temp.length() < 8)

				pinfield.setText(temp.concat(b.toString()));
			/**
			 * TODO: Refine @build() to use @Bundle
			 */
		}
	}

	private void callLogin() {

		String code = Utility.makeHashKey(pinfield.getText().toString());
		String targetCode = AppSettingsHelper.getApplicationPin(context);

		if (code.equals(targetCode)) {
			FragmentTransaction trans = getFragmentManager().beginTransaction();
			trans.replace(R.id.master_container, new OverviewFragment());
			trans.addToBackStack(null);
			trans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
			trans.commit();
		} else {
			pinfield.setText(null);
			Toast.makeText(context, "Wrong PIN. Please try again", Toast.LENGTH_SHORT).show();
		}

	}

	/**
	 * Rebuilds the View according to what changed null == rebuild everything.
	 * 
	 * See @RenderStatus for detailed information about render flags
	 * 
	 * @param info
	 */

	private void buildUI(int status) {

		if (status == 0) {

			pinfield = (EditText) view.findViewById(R.id.login_screen_pin);
			pinfield.setText(null);

			zero = (Button) view.findViewById(R.id.login_screen_button_zero);
			one = (Button) view.findViewById(R.id.login_screen_button_one);
			two = (Button) view.findViewById(R.id.login_screen_button_two);
			three = (Button) view.findViewById(R.id.login_screen_button_three);
			four = (Button) view.findViewById(R.id.login_screen_button_four);
			five = (Button) view.findViewById(R.id.login_screen_button_five);
			six = (Button) view.findViewById(R.id.login_screen_button_six);
			seven = (Button) view.findViewById(R.id.login_screen_button_seven);
			eight = (Button) view.findViewById(R.id.login_screen_button_eight);
			nine = (Button) view.findViewById(R.id.login_screen_button_nine);
			submit = (Button) view.findViewById(R.id.login_screen_button_submit);
			back = (Button) view.findViewById(R.id.login_screen_button_back);
			forgot = (Button) view.findViewById(R.id.login_screen_button_forgot);

			setOnClickListener(numpad);

			Intent intent = new Intent(context, AlarmService.class);
			context.startService(intent);

		}

	}

	/**
	 * @l will be @numpad on build and @null on destroy of the listeners
	 * 
	 * @param l
	 */
	private void setOnClickListener(OnClickListener l) {
		zero.setOnClickListener(l);
		one.setOnClickListener(l);
		two.setOnClickListener(l);
		three.setOnClickListener(l);
		four.setOnClickListener(l);
		five.setOnClickListener(l);
		six.setOnClickListener(l);
		seven.setOnClickListener(l);
		eight.setOnClickListener(l);
		nine.setOnClickListener(l);
		submit.setOnClickListener(l);
		back.setOnClickListener(l);
		forgot.setOnClickListener(l);
	}
}
