package androidlab.exercise3_1;

import java.sql.Date;
import java.sql.Time;

/**
 * Eclipse magic! Handling everything around the Reminder Object
 * 
 * @author LeGemiNi
 * 
 */
public class Reminder {

  private int ID;
  private String name;
  private Date date;
  private Time time;
  private String notes;
  private boolean mlp;
  private boolean completed;

  /**
   * @return the completed
   */
  public boolean isCompleted() {

	return completed;
  }

  /**
   * @param completed
   *          the completed to set
   */
  public void setCompleted(boolean completed) {

	this.completed = completed;
  }

  /**
   * @param mlp
   *          the mlp to set
   */
  public void setMlp(boolean mlp) {

	this.mlp = mlp;
  }

  /**
   * @return the iD
   */
  public int getID() {

	return ID;
  }

  public Reminder(String name, boolean completed, boolean mlp, Date date, Time time, String notes) {

	super();
	ID = -1;
	this.name = name;
	this.date = date;
	this.time = time;
	this.notes = notes;
	this.mlp = mlp;
	this.completed = completed;
  }

  public Reminder(int iD, String name, boolean completed, boolean mlp, Date date, Time time, String notes) {

	super();
	ID = iD;
	this.name = name;
	this.date = date;
	this.time = time;
	this.notes = notes;
	this.mlp = mlp;
	this.completed = completed;
  }

  /**
   * @param iD
   *          the iD to set
   */
  public void setID(int iD) {

	ID = iD;
  }

  /**
   * @return the name
   */
  public String getName() {

	return name;
  }

  /**
   * @param name
   *          the name to set
   */
  public void setName(String name) {

	this.name = name;
  }

  /**
   * @return the date
   */
  public Date getDate() {

	return date;
  }

  /**
   * @param date
   *          the date to set
   */
  public void setDate(Date date) {

	this.date = date;
  }

  /**
   * @return the time
   */
  public Time getTime() {

	return time;
  }

  /**
   * @param time
   *          the time to set
   */
  public void setTime(Time time) {

	this.time = time;
  }

  /**
   * @return the notes
   */
  public String getNotes() {

	return notes;
  }

  /**
   * @param notes
   *          the notes to set
   */
  public void setNotes(String notes) {

	this.notes = notes;
  }

  /**
   * @return the mlp
   */
  public boolean isMlp() {

	return mlp;
  }

}
