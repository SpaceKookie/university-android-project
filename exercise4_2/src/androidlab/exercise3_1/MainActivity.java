package androidlab.exercise3_1;

import java.util.ArrayList;

import samples.Samples;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.DataSetObserver;
import android.view.*;
import android.view.View.OnCreateContextMenuListener;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View.OnClickListener;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.*;
import androidlab.exercise3_1.adapter.DatabaseAdapter;
import androidlab.exercise3_1.model.ToDoItem;
import androidlab.exercise3_1.notification.RemindService;

/**
 * <b>This application is design for the API level >= 16.</b>
 * <hr>
 * <h1>Example solution - ToDo list manager (ex-03)</h1>
 * <h2>Main activity</h2>
 * This activity provides the main view of the ToDo list manager. Therefore it
 * contains a list view {@link MainActivity#listView} which shows all the ToDo
 * items. To customize the list view itself there exists an inner class
 * {@link ListViewAdapter}. This class extends from the {@link BaseAdapter} and
 * handles the behavior and also the items of the list view. The storage of the
 * ToDo items is implemented by a SQLite database. The access to this database
 * is done through a separated class {@link DatabaseAdapter} which provides for
 * each purpose a static method. This is so because there a several process
 * which try to interact with the database at the same time. The add new items
 * this activity also provides a button {@link MainActivity#addButton} that will
 * allows by clicking it to insert the data of a new item.
 * 
 * <hr>
 * 
 * Most of the required functionality on the exercise sheet is implemented in
 * the {@link MainActivity}. As said the activity contains a list view that
 * shows all available ToDo items. On the top of the screen the user has the
 * possibility to add a new item through a simple button. For editing or
 * deleting of items the user can either choose these actions in the context
 * menu by long pressing on an item or also by clicking the corresponding button
 * on each item view. Beyond the user can directly check or uncheck an item as
 * done and he can delete all items by selecting this action in the options
 * menu.
 * 
 * 
 * 
 * @see {@link ListViewAdapter}, {@link DatabaseAdapter}, {@link EditActivity}
 * @author Seminar 'Introduction to Android Smartphone Programming', University
 *         of Freiburg
 * @version 1.3
 **/
public class MainActivity extends Activity {

	/**
	 * Variable which is the identifier of the value which indicates whether the
	 * edit activity is called for the creation of a new item or for the update
	 * of an existing item.
	 **/
	public final static String REQUEST_CODE = "androidlab.exercise3_1.call_edit_action";
	/**
	 * Variable which indicates that the edit activity is open for the creation
	 * of a new item if it is put as extra to the intent with the identifier
	 * {@link MainActivity#REQUEST_CODE}.
	 **/
	public final static int ITEM_NEW = 1;
	/**
	 * Variable which indicates that the edit activity is open for the update of
	 * an existing item if it is put as extra to the intent with the identifier
	 * {@link MainActivity#REQUEST_CODE}.
	 **/
	public final static int ITEM_UPDATE = 2;
	/**
	 * Variable which indicates that the item which was edited in the edit
	 * activity was deleted. This value will be passed through the
	 * {@code resultCode} variable in the method
	 * {@link MainActivity#onActivityResult(int, int, Intent)} if this is the
	 * case.
	 * 
	 * @see {@link Activity#RESULT_CANCELED}, {@link Activity#RESULT_OK}
	 **/
	public final static int RESULT_DELETE = 2;

	/**
	 * Adapter of the list view which handles the behavior of the view and
	 * customizes the list view and its items.
	 */
	private ListViewAdapter listViewAdapter;
	/**
	 * List view which i contained by this activity and which should be handled
	 * by the {@link MainActivity#listViewAdapter}.
	 */
	private ListView listView;
	/** Button for adding new items */
	private Button addButton;

	/**
	 * Set the instance variables, adds an OnClickListener to the
	 * {@link MainActivity#addButton} that calls the method
	 * {@link MainActivity#newToDoItem()} and also add the items which are
	 * stored in the database to the list view (includes the creation of the
	 * {@link MainActivity#listViewAdapter}). For the latter the method
	 * {@link MainActivity#updateListViewDatabase()} is invoked. Finally the
	 * context menu will be enable for the list items.
	 * 
	 * @param savedInstanceState
	 *            {@code null} or the {@link Bundle} contains the data it most
	 *            recently supplied in
	 *            {@link Activity#onSaveInstanceState(Bundle)}.
	 * 
	 * @see {@link Activity#onCreate(Bundle)}, {@link ListViewAdapter}
	 **/
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_activity);
		addButton = (Button) findViewById(R.id.add_item);
		addButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				newToDoItem();
			}
		});
		listView = (ListView) findViewById(R.id.list_todos);
		updateListViewDatabase();
		registerForContextMenu(listView);
	}

	/**
	 * Updates the list view when the activity was restarted (calls
	 * {@link MainActivity#updateListViewDatabase()}).
	 * 
	 * @see {@link Activity#onRestart()}, {@link ListViewAdapter}
	 **/
	@Override
	protected void onRestart() {
		updateListViewDatabase();
		super.onRestart();
	}

	/**
	 * Updates the list view when the activity was resumed (calls
	 * {@link MainActivity#updateListViewDatabase()}).
	 * 
	 * @see {@link Activity#onResume()}, {@link ListViewAdapter}
	 **/
	@Override
	protected void onResume() {
		updateListViewDatabase();
		super.onResume();
	}

	/**
	 * Handles the selection in the options menu. In this activity two items are
	 * selectable if the developer mode is activated (see
	 * {@code res/values/app_settings.xml}) otherwise the user can only select
	 * the deletion of all items ({@link MainActivity#deleteAllToDoItems()}).If
	 * the dev mode is activated also sample data can be inserted in the
	 * database ({@link MainActivity#insertTestData()}).
	 * 
	 * @param item
	 *            The menu item that was selected.
	 * @return Return {@code false} to allow normal menu processing to proceed,
	 *         {@code true} to consume it here.
	 * 
	 * @see {@link Activity#onOptionsItemSelected(MenuItem)}
	 **/
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_delete_all:
			deleteAllToDoItems();
			break;
		case R.id.testdata:
			insertTestData();
			break;
		}
		return true;
	}

	/**
	 * Creates the options menu through the menu resource
	 * {@code R.menu.main_activity}. If the dev mode is not activated the item
	 * for inserting sample data will be removed.
	 * 
	 * @param menu
	 *            The options menu in which you place your items.
	 * @result Return always {@code true} so that the menu will be displayed.
	 * 
	 * @see {@link Activity#onCreateOptionsMenu(Menu)}
	 **/
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main_activity, menu);
		if (!getResources().getBoolean(R.bool.dev_mode))
			menu.removeItem(R.id.testdata);
		return true;
	}

	/**
	 * Handles the selection in the context menu which is enabled for each item
	 * of the list view {@link MainActivity#listView}. There are two possible
	 * selection in each context menu - firstly the editing of the item (
	 * {@link MainActivity#editToDoItemWith(int)}) and secondly the deletion of
	 * the item ({@link MainActivity#deleteToDoItemWith(int)}). Through the
	 * target view the identifier of the item can be received which is required
	 * for the editing or deletion of an item.
	 * 
	 * @param item
	 *            The context menu item that was selected.
	 * @result Return {@code false} to allow normal menu processing to proceed,
	 *         {@code true} to consume it here.
	 * 
	 * @see {@link Activity#onContextItemSelected(MenuItem)}
	 **/
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_delete:
			AdapterContextMenuInfo menuInfoDel = (AdapterContextMenuInfo) item
					.getMenuInfo();
			int toDelId = ((Integer) menuInfoDel.targetView.getTag())
					.intValue();
			this.deleteToDoItemWith(toDelId);
			break;
		case R.id.menu_edit:
			AdapterContextMenuInfo menuInfoEdit = (AdapterContextMenuInfo) item
					.getMenuInfo();
			int toEditId = ((Integer) menuInfoEdit.targetView.getTag())
					.intValue();
			this.editToDoItemWith(toEditId);
			break;
		}
		return true;
	}

	/**
	 * Creates the context menu through the menu resource
	 * {@code R.menu.context_main_menu}. Also the method sets the title of the
	 * context menu which contains the title of the corresponding ToDo item.
	 * Therefore the method
	 * {@link DatabaseAdapter#fetchToDoItemWith(int, Context)} will be invoked
	 * to receive this ToDo item which is the context of this menu.
	 * 
	 * @param menu
	 *            The context menu that is being built
	 * @param v
	 *            The view for which the context menu is being built
	 * @param menuInfo
	 *            Extra information about the item for which the context menu
	 *            should be shown. This information will vary depending on the
	 *            class of v.
	 * 
	 * @see {@link Activity#onCreateContextMenu(ContextMenu, View, ContextMenuInfo)}
	 *      , {@link DatabaseAdapter}
	 **/
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.context_main_menu, menu);
		int id = ((Integer) ((AdapterContextMenuInfo) menuInfo).targetView
				.getTag()).intValue();
		ToDoItem todo = DatabaseAdapter.fetchToDoItemWith(id, this);
		String title = String.format("%s '%.8s %s'",
				getResources().getString(R.string.todo_item), todo.getTitle(),
				getResources().getString(R.string.dots));
		menu.setHeaderTitle(title);
	}

	/**
	 * Handles the returning from the edit activity {@link EditActivity}. For
	 * every combination of each possible request
	 * {@link MainActivity#REQUEST_CODE} (where the values can be
	 * {@link MainActivity#ITEM_NEW} and {@link MainActivity#ITEM_UPDATE}) as
	 * well as for each possible result (where the be
	 * {@link Activity#RESULT_CANCELED}, {@link Activity#RESULT_OK} and
	 * {@link MainActivity#RESULT_DELETE}) the method shows a toast message with
	 * the information what happens. If an existing item was deleted or updated,
	 * or if a new item was created than the remind service
	 * {@link RemindService} will be informed about the changes via an intent
	 * which invokes the service.
	 * 
	 * @param requestCode
	 *            The integer request code originally supplied to
	 *            startActivityForResult(), allowing you to identify who this
	 *            result came from.
	 * @param resultCode
	 *            The integer result code returned by the child activity through
	 *            its setResult().
	 * @param data
	 *            An Intent, which can return result data to the caller (various
	 *            data can be attached to Intent "extras").
	 * 
	 * @see {@link Activity#onActivityResult(int, int, Intent)},
	 *      {@link MainActivity#newToDoItem()},
	 *      {@link MainActivity#editToDoItemWith(int)}
	 **/
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == ITEM_NEW && resultCode == RESULT_OK) {
			Toast.makeText(getApplicationContext(),
					getString(R.string.new_created), Toast.LENGTH_LONG).show();
			int id = data.getIntExtra(ToDoItem.ID, -1);
			if (id != -1) {
				Intent intent = new Intent(this, RemindService.class)
						.setAction(RemindService.UPDATE_REMIND);
				startService(intent);
			}
		} else if (requestCode == ITEM_NEW && resultCode == RESULT_CANCELED) {
			Toast.makeText(getApplicationContext(),
					getString(R.string.new_canceled), Toast.LENGTH_LONG).show();
		} else if (requestCode == ITEM_NEW && resultCode == RESULT_DELETE) {
			Toast.makeText(getApplicationContext(),
					getString(R.string.new_canceled), Toast.LENGTH_LONG).show();
		} else if (requestCode == ITEM_UPDATE && resultCode == RESULT_OK) {
			Toast.makeText(getApplicationContext(),
					getString(R.string.update_saved), Toast.LENGTH_LONG).show();
			int id = data.getIntExtra(ToDoItem.ID, -1);
			if (id != -1) {
				Intent intent = new Intent(this, RemindService.class)
						.setAction(RemindService.UPDATE_REMIND);
				startService(intent);
			}
		} else if (requestCode == ITEM_UPDATE && resultCode == RESULT_CANCELED) {
			Toast.makeText(getApplicationContext(),
					getString(R.string.update_canceled), Toast.LENGTH_LONG)
					.show();
		} else if (requestCode == ITEM_UPDATE && resultCode == RESULT_DELETE) {
			Toast.makeText(getApplicationContext(),
					getString(R.string.update_deleted), Toast.LENGTH_LONG)
					.show();
			int id = data.getIntExtra(ToDoItem.ID, -1);
			if (id != -1) {
				Intent intent = new Intent(this, RemindService.class)
						.setAction(RemindService.UPDATE_REMIND);
				startService(intent);
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	/**
	 * Method will fetch all ToDo items from the database by invoking the method
	 * {@link DatabaseAdapter#fetchAllToDoItems(Context)}. These items will be
	 * given to the {@link MainActivity#listViewAdapter}. This will inform all
	 * observers so that all those can refresh there view (see
	 * {@link ListViewAdapter#setItems(ArrayList)}).
	 * 
	 * @see {@link ListViewAdapter}, {@link DatabaseAdapter}
	 **/
	public void updateListViewDatabase() {
		ArrayList<ToDoItem> list = DatabaseAdapter.fetchAllToDoItems(this);
		if (listViewAdapter == null) {
			listViewAdapter = new ListViewAdapter(this, list);
			listView.setAdapter(listViewAdapter);
		} else {
			listViewAdapter.setItems(list);
		}

	}

	/**
	 * Method will start the activity where an item can be edited (
	 * {@link EditActivity}). In this case the intent which will be passed to
	 * the activity does contain the information that a new item should be
	 * edited. The goal during the start of the edit activity is that the result
	 * will be passed back to this activity (see
	 * {@link Activity#startActivityForResult(Intent, int)}).
	 * 
	 * @see {@link EditActivity},
	 *      {@link MainActivity#onActivityResult(int, int, Intent)},
	 *      {@link MainActivity#ITEM_NEW}, {@link MainActivity#REQUEST_CODE}
	 **/
	public void newToDoItem() {
		Intent intent = new Intent(this, EditActivity.class);
		intent.putExtra(REQUEST_CODE, ITEM_NEW);
		startActivityForResult(intent, ITEM_NEW);
	}

	/**
	 * Method will start the activity where an item can be edited (
	 * {@link EditActivity}). In this case the intent which will be passed to
	 * the activity does contain the information that a item should be edited
	 * which exists already. Therefore following extra data are added to the
	 * intent. The {@link MainActivity#REQUEST_CODE} which contains the value
	 * {@link MainActivity#ITEM_UPDATE} as well as the {@link ToDoItem#ID} where
	 * the value is the given identifier. The goal during the start of the edit
	 * activity is that the result will be passed back to this activity (see
	 * {@link Activity#startActivityForResult(Intent, int)}).
	 * 
	 * @param id
	 *            The identifier of the ToDo item which should be edited.
	 * 
	 * @see {@link EditActivity},
	 *      {@link MainActivity#onActivityResult(int, int, Intent)},
	 *      {@link MainActivity#ITEM_UPDATE}, {@link MainActivity#REQUEST_CODE}
	 **/
	public void editToDoItemWith(int id) {
		Intent intent = new Intent(this, EditActivity.class);
		intent.putExtra(REQUEST_CODE, ITEM_UPDATE);
		intent.putExtra(ToDoItem.ID, id);
		startActivityForResult(intent, ITEM_UPDATE);
	}

	/**
	 * Will delete the ToDo item from the database which has the given
	 * identifier. Therefore it calls the method
	 * {@link DatabaseAdapter#deleteToDoItemWith(int, Context)}, refreshes the
	 * list view and informs the {@link RemindService} that changes happened by
	 * starting the this service.
	 * 
	 * @param id
	 *            The identifier of the ToDo item which should be deleted.
	 * 
	 * @see {@link ToDoItem}, {@link DatabaseAdapter}, {@link RemindService}
	 **/
	public void deleteToDoItemWith(int id) {
		DatabaseAdapter.deleteToDoItemWith(id, this);
		updateListViewDatabase();
		Intent intent = new Intent(this, RemindService.class)
				.setAction(RemindService.UPDATE_REMIND);
		startService(intent);
	}

	/**
	 * Will delete all the ToDo items from the database. Therefore it calls the
	 * method {@link DatabaseAdapter#deleteAllToDoItems(Context)}, refreshes the
	 * list view and informs the {@link RemindService} that all items were
	 * deleted by starting the this service.
	 * 
	 * @see {@link DatabaseAdapter}, {@link RemindService}
	 **/
	public void deleteAllToDoItems() {
		DatabaseAdapter.deleteAllToDoItems(this);
		updateListViewDatabase();
		Intent intent = new Intent(this, RemindService.class)
				.setAction(RemindService.DELETE_REMIND);
		startService(intent);
	}

	/**
	 * Will update the done status of the ToDo item in the database which has
	 * the given identifier. Therefore it calls the method
	 * {@link DatabaseAdapter#updateToDoItemWith(int, boolean, Context)},
	 * refreshes the list view and informs the {@link RemindService} that
	 * changes happened by starting the this service.
	 * 
	 * @param id
	 *            The identifier of the ToDo item for which the done status
	 *            should be changed.
	 * @param checkStatus
	 *            Boolean value to which the done status of the ToDo item should
	 *            be changed.
	 * 
	 * @see {@link DatabaseAdapter}, {@link RemindService}
	 **/
	public void checkToDoItemWith(int id, boolean checkStatus) {
		DatabaseAdapter.updateToDoItemWith(id, checkStatus, this);
		updateListViewDatabase();
		Intent intent = new Intent(this, RemindService.class)
				.setAction(RemindService.UPDATE_REMIND);
		startService(intent);
	}

	/**
	 * If the developer mode is activated (see
	 * {@code res/values/app_settings.xml}) the method will insert random sample
	 * data in the database {@link DatabaseAdapter#insertTestdata(Context)} and
	 * also refresh the list view, so that the new items will be shown. Finally
	 * the remind service {@link RemindService} will be informed about the
	 * change via an intent which invokes the service.
	 * 
	 * @see {@link DatabaseAdapter}, {@link Samples}, {@link RemindService}
	 **/
	private void insertTestData() {
		if (getResources().getBoolean(R.bool.dev_mode)) {
			DatabaseAdapter.insertTestdata(this);
			updateListViewDatabase();
			Intent intent = new Intent(this, RemindService.class)
					.setAction(RemindService.UPDATE_REMIND);
			startService(intent);
		}
	}

	/**
	 * The {@link ListViewAdapter} extends from the {@link BaseAdapter} and also
	 * implements the {@link ListAdapter} interface so that this class handles
	 * the usage of a list view which shows a customized view of a complex
	 * datatype. The most method are inherit and were adjusted so that e.g. the
	 * underlying data set was respected. As said, this class contains a data
	 * set {@link ListViewAdapter#data} which stores the ToDo items which should
	 * shown in the list view. Further there exists a list for the observers
	 * which are registered to this adapter and which want to be notified if
	 * changes happen. In addition also a reference to the {@link MainActivity}
	 * is stored, so that a single item in the list view has access to the
	 * public methods of that activity.
	 **/
	public class ListViewAdapter extends BaseAdapter implements ListAdapter,
			OnCreateContextMenuListener {

		/**
		 * Data source of the list view. For all objects which are in this
		 * {@link ArrayList} will be an entry in the list view created.
		 */
		private ArrayList<ToDoItem> data = new ArrayList<ToDoItem>();
		/** List of the registered Observers */
		private ArrayList<DataSetObserver> observers = new ArrayList<DataSetObserver>();
		/**
		 * The {@link MainActivity} that contains the list view for which the
		 * this adapter is used and which provides public access to the methods
		 * {@link MainActivity#checkToDoItemWith(int, boolean)},
		 * {@link MainActivity#deleteToDoItemWith(int)} and
		 * {@link MainActivity#editToDoItemWith(int)}.
		 **/
		private MainActivity activity;

		/**
		 * Constructor of a {@link ListViewAdapter} which takes the reference to
		 * the {@link MainActivity} in which this Adapter is used so that the
		 * items of the list view has access to the public methods of the
		 * {@link MainActivity}.
		 * 
		 * @param activity
		 *            The {@link MainActivity} where the methods
		 *            {@link MainActivity#checkToDoItemWith(int, boolean)},
		 *            {@link MainActivity#deleteToDoItemWith(int)} and
		 *            {@link MainActivity#editToDoItemWith(int)} exist.
		 **/
		private ListViewAdapter(MainActivity activity) {
			this.activity = activity;
		}

		/**
		 * Constructor of a {@link ListViewAdapter} which takes the reference to
		 * the {@link MainActivity} in which this Adapter is used so that the
		 * items of the list view has access to the public methods of the
		 * {@link MainActivity} as well as a list of ToDo items which should be
		 * the data set of the list view adapter.
		 * 
		 * @param activity
		 *            The {@link MainActivity} where the methods
		 *            {@link MainActivity#checkToDoItemWith(int, boolean)},
		 *            {@link MainActivity#deleteToDoItemWith(int)} and
		 *            {@link MainActivity#editToDoItemWith(int)} exist.
		 * @param storage
		 *            List of ToDo items which should be the data set of this
		 *            list view adapter.
		 **/
		private ListViewAdapter(MainActivity activity,
				ArrayList<ToDoItem> storage) {
			this.activity = activity;
			setItems(storage);
		}

		/**
		 * Specifies how many items in the data set are.
		 * 
		 * @return Count of ToDo items in the data set
		 *         {@link ListViewAdapter#data}.
		 * 
		 * @see {@link Adapter#getCount()}, {@link ListViewAdapter#data}
		 **/
		@Override
		public int getCount() {
			return data.size();
		}

		/**
		 * Get the ToDo item associated with the specified position in the data
		 * set.
		 * 
		 * @param position
		 *            Position of the item whose data we want within the
		 *            adapter's data set.
		 * 
		 * @return The ToDo item at the specified position. If this index does
		 *         not exists the method will return {@code null}.
		 * 
		 * @see {@link Adapter#getItem(int)}, {@link ListViewAdapter#data},
		 *      {@link ToDoItem}
		 **/
		@Override
		public ToDoItem getItem(int position) {
			if (position >= data.size())
				return null;
			return data.get(position);
		}

		/**
		 * Get the ToDo item id associated with the specified position in the
		 * list.
		 * 
		 * @param position
		 *            The position of the ToDo item within the adapter's data
		 *            {@link ListViewAdapter#data} set whose row id we want.
		 * 
		 * @return The id ({@link ToDoItem#getId()}) of the ToDo item at the
		 *         specified position.
		 * 
		 * @see {@link Adapter#getItemId(int)}, {@link ToDoItem},
		 *      {@link ListViewAdapter#data}
		 **/
		@Override
		public long getItemId(int position) {
			return data.get(position).getId();
		}

		/**
		 * Method returns the type of view of a ToDo item specific position in
		 * the data set. But this Adapter does only use one type of View, so the
		 * result is always 0.
		 * 
		 * @param position
		 *            The position of the item within the adapter's data set
		 *            whose view type we want.
		 * 
		 * @return Always 0, because there is only one type of View.
		 * 
		 * @see {@link BaseAdapter#getItemViewType(int)}
		 **/
		@Override
		public int getItemViewType(int position) {
			return 0;
		}

		/**
		 * Get a View that displays the ToDo item at the specified position in
		 * the data set {@link ListViewAdapter#data}. For creating the view the
		 * layout {@code res/layout/list_view_item.xml} is used. All the fields
		 * of this layout are filled with corresponding data and the background
		 * is colored depending firstly by the done status and secondly by the
		 * remind date (grey if the ToDo item is done, red if the remind date is
		 * in the past, otherwise white). Anonymous {@link OnClickListener} a
		 * set to the CheckBox which indicates the done status and to the
		 * buttons for deleting and editing the ToDo item. All those
		 * {@link OnClickListener} will call the associated method in the
		 * {@link ListViewAdapter#activity} (see {@link MainActivity}). Finally
		 * the tag of the view which will be returned is set to the identifier
		 * of the ToDo item so that especially the context menu knows for which
		 * item this is the context.
		 * 
		 * @param position
		 *            The position of the ToDo item within the adapter's data
		 *            set of the item whose view should display.
		 * @param convertView
		 *            The old view to reuse, if possible.
		 * @param parent
		 *            The parent that this view will eventually be attached to.
		 * 
		 * @return A View corresponding to the ToDo item at the specified
		 *         position in the data source {@link ListViewAdapter#data}.
		 * 
		 * @see {@link Adapter#getView(int, View, ViewGroup)}, {@link ToDoItem},
		 *      {@link ListViewAdapter#data}
		 **/
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			final ToDoItem todo = data.get(position);
			if (convertView == null) {
				LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.list_view_item, parent,
						false);
			}
			LinearLayout bg = (LinearLayout) convertView
					.findViewById(R.id.list_view_item);
			if (todo.isDone()) {
				bg.setBackgroundResource(R.color.softgrey);
			} else if (!todo.isAfterNow() && !todo.isDone()
					&& todo.shouldRemind()) {
				bg.setBackgroundResource(R.color.softred);
			} else {
				bg.setBackgroundResource(R.color.white);
			}
			CheckBox done = (CheckBox) convertView
					.findViewById(R.id.item_checked);
			done.setChecked(todo.isDone());
			done.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					int toCheckId = todo.getId();
					CheckBox checkbox = (CheckBox) v;
					activity.checkToDoItemWith(toCheckId, checkbox.isChecked());
				}
			});
			TextView title = (TextView) convertView
					.findViewById(R.id.item_title);
			title.setText(todo.getTitle());
			TextView datetime = (TextView) convertView
					.findViewById(R.id.item_datetime);
			if (todo.shouldRemind()) {
				datetime.setText(String.format("%1$tF%n%1$tR", todo.getDate()
						.getTime()));
			} else {
				datetime.setText("");
			}
			Button delete = (Button) convertView.findViewById(R.id.item_delete);
			delete.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					int toDelId = todo.getId();
					activity.deleteToDoItemWith(toDelId);
				}
			});
			Button edit = (Button) convertView.findViewById(R.id.item_edit);
			edit.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					int toEditId = todo.getId();
					activity.editToDoItemWith(toEditId);
				}
			});
			convertView.setTag(todo.getId());
			convertView.setOnCreateContextMenuListener(this);
			return convertView;
		}

		/**
		 * Method will always return 1 because this list view has only one type
		 * of {@link View} that will be created by the method
		 * {@link ListViewAdapter#getView(int, View, ViewGroup)}.
		 * 
		 * @return Always 1.
		 * 
		 * @see {@link BaseAdapter#getViewTypeCount()}
		 **/
		@Override
		public int getViewTypeCount() {
			return 1;
		}

		/**
		 * The underlying data, in this case the ToDo items have stable
		 * {@code id}s. These unique identifiers are the id of the items
		 * {@link ToDoItem#getId()}.
		 * 
		 * @return Always {@code true}.
		 * 
		 * @see {@link BaseAdapter#hasStableIds()}
		 **/
		@Override
		public boolean hasStableIds() {
			return true;
		}

		/**
		 * Checks whether the underlying data set is empty or not.
		 * 
		 * return {@code true} if the {@link ArrayList} is empty otherwise
		 * {@code false}.
		 * 
		 * @see {@link BaseAdapter#isEmpty()}, {@link ListViewAdapter#data}
		 **/
		@Override
		public boolean isEmpty() {
			return this.data.isEmpty();
		}

		/**
		 * Register an observer that is called when changes happen to the data
		 * used by this adapter. That means that this given observer will be
		 * added to the observer list {@link ListViewAdapter#observers}.
		 * 
		 * @see {@link BaseAdapter#registerDataSetObserver(DataSetObserver)}
		 **/
		@Override
		public void registerDataSetObserver(DataSetObserver observer) {
			observers.add(observer);

		}

		/**
		 * Unregister an observer that has previously been registered with this
		 * adapter via
		 * {@link ListViewAdapter#registerDataSetObserver(DataSetObserver)}.
		 * That means that this given observer will be removed from the observer
		 * list {@link ListViewAdapter#observers}.
		 * 
		 * @see {@link BaseAdapter#unregisterDataSetObserver(DataSetObserver)}
		 **/
		@Override
		public void unregisterDataSetObserver(DataSetObserver observer) {
			observers.remove(observer);

		}

		/**
		 * Notifies all observers in the {@link ListViewAdapter#observers} list
		 * that some changes happened by calling the method
		 * {@link DataSetObserver#onChanged()}.
		 **/
		public void notifyObservers() {
			for (int i = 0; i < observers.size(); ++i) {
				observers.get(i).onChanged();
			}
		}

		/**
		 * All entries of the list view are enabled. That is implemented in this
		 * way because it is a workaround so that it is possible to open a
		 * context menu for each item of a customized list view.
		 * 
		 * @return Always {@code true}.
		 * 
		 * @see {@link BaseAdapter#areAllItemsEnabled()}
		 **/
		@Override
		public boolean areAllItemsEnabled() {
			return true;
		}

		/**
		 * Each entry of the list view is disabled. That is implemented in this
		 * way because it is a workaround so that it is possible to open a
		 * context menu for each item of a customized list view.
		 * 
		 * @param position
		 *            Index of the item
		 * @return Always {@code false}.
		 * 
		 * @see {@link BaseAdapter#isEnabled(int)}
		 **/
		@Override
		public boolean isEnabled(int position) {
			return false;
		}

		/**
		 * Nothing should happen when a context menu should created in the list
		 * view. This will be done by the {@link MainActivity}. That is
		 * implemented in this way because it is a workaround so that it is
		 * possible to open a context menu for each item of a customized list
		 * view.
		 * 
		 * @param menu
		 *            The context menu that is being built
		 * @param v
		 *            The view for which the context menu is being built
		 * @param menuInfo
		 *            Extra information about the item for which the context
		 *            menu should be shown. This information will vary depending
		 *            on the class of v.
		 * 
		 * @see {@link OnCreateContextMenuListener#onCreateContextMenu(ContextMenu, View, ContextMenuInfo)}
		 **/
		@Override
		public void onCreateContextMenu(ContextMenu menu, View view,
				ContextMenuInfo menuInfo) {
			// Do nothing, because ContextMenu will be created by the
			// MainActivity.
		}

		/**
		 * Sets the given ToDo items as data source for the list view and
		 * notifies the observers that the data source was updated. So all
		 * Observers can refresh, e.g. their view.
		 * 
		 * @param items
		 *            List of ToDo items which should be the new data source.
		 * 
		 * @see {@link ListViewAdapter#notifyObservers()}
		 **/
		public void setItems(ArrayList<ToDoItem> items) {
			this.data = items;
			notifyObservers();
		}

	}
}
