package androidlab.exercise3_1;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

public class FragmentActivityHandler extends FragmentActivity {

    public void makeMagicHappen() {

	FragmentManager manager = getSupportFragmentManager();
	LocationFragment fragment = new LocationFragment();
	fragment.show(manager, "location_editor");
    }

    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	
	LocationFragment fragment = new LocationFragment();

	if (savedInstanceState == null) {
	    getSupportFragmentManager().beginTransaction()
		    .add(android.R.id.content, fragment).commit();
	} else {
	    fragment = (LocationFragment) getSupportFragmentManager()
		    .findFragmentById(android.R.id.content);
	}

    }

}
