package samples;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;
import java.util.Scanner;

import android.content.Context;

import androidlab.exercise3_1.model.*;

/**
 * <h2>Random Generator (for testing reasons)</h2> Class provides several static
 * methods which generates random values. Main method is the generation of an
 * ArrayList which contains random ToDo item {@link ToDoItem}. These can be used
 * as sample data for the testing.
 * 
 * @see {@link Calendar}, {@link ToDoItem}
 * @author Seminar 'Introduction to Android Smartphone Programming', University
 *         of Freiburg
 * @version 1.3
 **/
public class Samples {

	/**
	 * Generates a list of maximal 10 random ToDo items ({@link ToDoItem}).
	 * 
	 * @param context
	 *            Context where the method is invoke
	 * @return List of randomly generated ToDo items.
	 */
	public static ArrayList<ToDoItem> generateListOfToDoItems(Context context) {
		int count = genInt(11);
		ArrayList<ToDoItem> todos = new ArrayList<ToDoItem>();
		for (int i = 0; i < count; i++) {
			try {
				todos.add(new ToDoItem(i, genTitle(context), genText(
						genInt(21), context), genTrueOrFalse(),
						genTrueOrFalse(), genCalInBetweenHour()
								.getTimeInMillis()));
			} catch (Exception e) {
			}
		}
		return todos;
	}

	/**
	 * Generates randomly 1 or -1.
	 * 
	 * @return Random positive or negative integer value (1,-1)
	 */
	private static int genPosOrNeg() {
		Random random = new Random();
		return random.nextInt(2) == 1 ? 1 : -1;
	}

	/**
	 * Generates randomly {@code true} or {@code false}.
	 * 
	 * @return Random boolean value ({@code true}, {@code false})
	 */
	private static boolean genTrueOrFalse() {
		Random random = new Random();
		return random.nextInt(2) == 1;
	}

	/**
	 * Generates randomly an integer value between inclusive 0 and {@code upTo}.
	 * 
	 * @param upTo
	 *            Value so that the maximal result value can be {@code upTo - 1}
	 *            .
	 * @return Random integer value. Interval: [0, {@code upTo - 1}]
	 */
	private static int genInt(int upTo) {
		Random random = new Random();
		return random.nextInt(upTo);
	}

	/**
	 * Generates a calendar ({@link Calendar}) with a random date. This method
	 * does not respect any time limitations.
	 * 
	 * @return Random calendar.
	 */
	@SuppressWarnings("unused")
	private static Calendar genCal() {
		Random random = new Random();
		Calendar cal = Calendar.getInstance();
		cal.set(random.nextInt(2501), random.nextInt(12) + 1,
				random.nextInt(31) + 1, random.nextInt(24), random.nextInt(60),
				random.nextInt(60));
		return cal;
	}

	/**
	 * Generates a calendar ({@link Calendar}) with a random date. This method
	 * does respect a time limitations, so that the maximal generated date is 30
	 * minutes after now and the minimal date is 30 minutes before now.
	 * 
	 * @return Random calendar with is max 30 min after and min 30 min before
	 *         now.
	 */
	private static Calendar genCalInBetweenHour() {
		Random random = new Random();
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MINUTE, genPosOrNeg() * random.nextInt(31));
		return cal;
	}

	/**
	 * Returns a random proverb which is stored in the file
	 * {@code assets/proverbs.txt}.
	 * 
	 * @param context
	 *            Context of the application, so that the method has access to
	 *            the {@code assets}-folder.
	 * @return Random String which contains a short proverb.
	 */
	private static String genTitle(Context context) {
		String result = "Hello World";
		Random random = new Random();
		int treshold = random.nextInt(12530);
		InputStream fs = null;
		InputStreamReader sr = null;
		BufferedReader br = null;

		try {
			fs = context.getAssets().open("proverbs.txt");
			sr = new InputStreamReader(fs);
			br = new BufferedReader(sr);
			for (int i = 0; i < treshold; ++i)
				br.readLine();
			result = br.readLine();
		} catch (Exception e) {
		}
		if (br != null)
			try {
				br.close();
			} catch (IOException e) {
			}
		if (sr != null)
			try {
				sr.close();
			} catch (IOException e) {
			}
		if (fs != null)
			try {
				fs.close();
			} catch (IOException e) {
			}
		return result;
	}

	/**
	 * Returns a random lore ipsum text which is approximately {@code caWords}
	 * words long. Therefore the method takes the words from the file
	 * {@code assets/loreipsum.txt}.
	 * 
	 * @param caWords
	 *            Defines how many words the result text should contain.
	 * @param context
	 *            Context of the application, so that the method has access to
	 *            the {@code assets}-folder.
	 * @return Random String which contains a lore ipsum text which contains the
	 *         given number of words.
	 */
	private static String genText(int caWords, Context context) {
		StringBuilder result = new StringBuilder();
		Scanner sc = null;
		try {
			sc = new Scanner(context.getAssets().open("loreipsum.txt"));
			sc.useDelimiter(" ");
			int count = 0;
			while (sc.hasNext() && count <= caWords) {
				if (result.length() != 0)
					result.append(" ");
				result.append(sc.next());
				count++;
			}
		} catch (Exception e) {
		}
		if (sc != null)
			try {
				sc.close();
			} catch (Exception e) {
			}
		if (result.charAt(result.length() - 1) != ".".charAt(0))
			result.append(".");
		return result.toString();

	}
}
