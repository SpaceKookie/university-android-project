package androidlab.exercise6_1.data;

/**
 * Singleton class to manage the database output (needed to make it accessible in the service
 * 
 * @author Konstantin
 *
 */
public class EvilMananger {

  private SQLOfEvil evil_table;


  private static EvilMananger instance = null;


  public static EvilMananger getInstance() {
        if (instance == null) {
          instance = new EvilMananger();
        }
        return instance;
  }

  public SQLOfEvil getDatabase() {
        return evil_table;
  }

  public void setEvil_table(SQLOfEvil table_reminders) {
        this.evil_table = table_reminders;
  }

  private EvilMananger() {
  }

}