package androidlab.exercise5_1;

import android.app.Activity;
import android.media.MediaPlayer;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.MotionEvent;

public class DiceActivity extends Activity {

  // The OpenGL View
  private GLSurfaceView glSurface;
  private OpenGLRenderer render;
  private MediaPlayer audio;

  
  /**
   * 
   * Creates the application, initializes the OpenGL surface and renderer and also creates the MedaPlayer object
   * 
   */
  @Override
  protected void onCreate(Bundle savedInstanceState) {

	super.onCreate(savedInstanceState);

	glSurface = new GLSurfaceView(this);
	glSurface.setRenderer(render = new OpenGLRenderer(getBaseContext()));
	audio = MediaPlayer.create(this, R.raw.sound);
	audio.setLooping(false);

	setContentView(glSurface);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {

	return false;
  }

  @Override
  protected void onResume() {

	super.onResume();
	glSurface.onResume();
  }

  @Override
  protected void onPause() {

	super.onPause();
	glSurface.onPause();
  }

  // Will register Touch Events. Only "UP" events (press-releases) will be used though
  @Override
  public boolean dispatchTouchEvent(MotionEvent event) {

	super.dispatchTouchEvent(event);
	if (event.getAction() == MotionEvent.ACTION_UP) {

	  if (OpenGLRenderer.rotatingOfDOOMFire) {
		audio.start();
	  }
	  render.touchy();
	}

	return true;
  }

}
