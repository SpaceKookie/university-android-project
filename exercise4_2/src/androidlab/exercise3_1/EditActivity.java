package androidlab.exercise3_1;

import java.util.Calendar;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.text.format.DateFormat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.TimePicker;
import androidlab.exercise3_1.adapter.DatabaseAdapter;
import androidlab.exercise3_1.model.ToDoItem;

/**
 * <h2>Activity which provides the possiblity to edit a ToDo item</h2> This
 * activity allows the user to edit a new ToDo item or a ToDo item that already
 * exists. Therefore the layout contains several input elements
 * {@code res/layout/edit_activity.xml} which correspond to the properties of a
 * ToDo item and which which are processed by this activity. Thus this class
 * includes the title {@link EditActivity#title}, the description
 * {@link EditActivity#description}, the done status {@link EditActivity#done},
 * the remind status {@link EditActivity#remind} and also the remind date
 * {@link EditActivity#remindCal}. To change the remind date respectively the
 * remind time two {@link Dialog}s are used (
 * {@link EditActivity#datePickerFragment},
 * {@link EditActivity#timePickerFragment}). To receive the choosen date and
 * time the activity implements the {@link OnDateSetListener} and the
 * {@link OnTimeSetListener}. Further there exists also a TextView
 * {@link EditActivity#remindInformation} which shows the information about the
 * remind status (shows that the ToDo item is already done, that the remind
 * functionality is disabled or the date and the time of the occurrence). In
 * order to know which ToDo item is processed also the identifier
 * {@link EditActivity#id} of the item exists (this will be -1 if a new item is
 * processed). The inserted data should be stored if the user presses the back
 * button ({@link EditActivity#onBackPressed()}) into the database through the
 * {@link DatabaseAdapter}.
 * 
 * @see {@link Activity}, {@link OnCheckListener}, {@link OnCheckListener},
 *      {@link DialogFragment}, {@link Calendar}, {@link OnDateSetListener},
 *      {@link OnTimeSetListener}, {@link DatabaseAdapter}, {@link MainActivity}
 *      , {@link MainActivity#onActivityResult(int, int, Intent)}
 * @author Seminar 'Introduction to Android Smartphone Programming', University
 *         of Freiburg
 * @version 1.3
 **/
public class EditActivity extends Activity implements
	DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    private RadioButton remind;
    private RadioButton location;

    /**
     * Nested class which implements the {@link OnCheckListener} interface. This
     * will be set to the two CheckBoxes {@link EditActivity#remind} and
     * {@link EditActivity#done} so that the activity can react to such changes.
     **/
    private class OnCheckListener implements OnCheckedChangeListener {

	/**
	 * Called when the status of the CheckBox for which this is a Listener
	 * was changed. To show directly which impact the new setting has, the
	 * input elements are dis- or enabled depending an their value (see
	 * {@link EditActivity#setupDisabilityOfInputElements()}).
	 * 
	 * @see {@link OnCheckedChangeListener#onCheckedChanged(CompoundButton, boolean)}
	 **/
	@Override
	public void onCheckedChanged(CompoundButton buttonView,
		boolean isChecked) {

	    setupDisabilityOfInputElements();

	    // RadioBox logic
	    if (location.isDirty()) {
		if (location.isChecked()) {
		    remind.setChecked(false);
		}

	    }

	    if (remind.isDirty()) {
		if (remind.isChecked()) {
		    location.setChecked(false);
		}

	    }

	    if (done.isChecked()) {
		remind.setChecked(false);
		remind.setEnabled(false);
		location.setChecked(false);
		location.setEnabled(false);

	    }

	}
    }

    /**
     * Instance of a {@link DialogFragment} which allows to show a Dialog to set
     * the time of the remind occurrence.
     */

    private DialogFragment timePickerFragment = new DialogFragment() {

	/**
	 * Method called when the Dialog should be created. It will set the
	 * showed time of the Dialog to the time when the remind occurrence is.
	 * With these information a new {@link TimePickerDialog} will be created
	 * and showed on the screen.
	 * 
	 * @param savedInstanceState
	 *            The last saved instance state of the Fragment, or null if
	 *            this is a freshly created Fragment.
	 * @result Return a new Dialog instance to be displayed by the Fragment.
	 * 
	 * @see {@link DialogFragment#onCreateDialog(Bundle)}
	 **/
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

	    int hour = remindCal.get(Calendar.HOUR_OF_DAY);
	    int minute = remindCal.get(Calendar.MINUTE);
	    return new TimePickerDialog(getActivity(), EditActivity.this, hour,
		    minute, DateFormat.is24HourFormat(getActivity()));
	}

    };

    /**
     * Instance of a {@link DialogFragment} which allows to show a Dialog to set
     * the date of the remind occurrence.
     */
    private DialogFragment datePickerFragment = new DialogFragment() {

	/**
	 * Method called when the Dialog should be created. It will set the
	 * showed date of the Dialog to the date when the remind occurrence is.
	 * With these information a new {@link DatePickerDialog} will be created
	 * and showed on the screen.
	 * 
	 * @param savedInstanceState
	 *            The last saved instance state of the Fragment, or null if
	 *            this is a freshly created Fragment.
	 * @result Return a new Dialog instance to be displayed by the Fragment.
	 * 
	 * @see {@link DialogFragment#onCreateDialog(Bundle)}
	 **/
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

	    int year = remindCal.get(Calendar.YEAR);
	    int month = remindCal.get(Calendar.MONTH);
	    int day = remindCal.get(Calendar.DAY_OF_MONTH);
	    return new DatePickerDialog(getActivity(), EditActivity.this, year,
		    month, day);
	}
    };

    /**
     * Identifier of the processed ToDo item (if this is a new ToDo item the
     * value of this variable is -1).
     */
    private int id = -1;
    private Calendar remindCal = Calendar.getInstance();
    private EditText title;
    private EditText description;
    private CheckBox done;
    private Button timeSetup;
    private Button dateSetup;
    private TextView remindInformation;

    private Button customPos;
    private TextView gpsInformation;

    /**
     * Called when the {@link TimePickerDialog} is finished which was created by
     * the {@link EditActivity#timePickerFragment}. So the method can store the
     * inputed values (hour, minute) in the {@link EditActivity#remindCal}
     * calendar.
     * 
     * @param view
     *            The view associated with this listener.
     * @param hourOfDay
     *            The hour that was set.
     * @param minute
     *            The minute that was set.
     * 
     * @see {@link OnTimeSetListener#onTimeSet(TimePicker, int, int)}
     **/
    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

	remindCal.set(Calendar.HOUR_OF_DAY, hourOfDay);
	remindCal.set(Calendar.MINUTE, minute);
	remindCal.set(Calendar.SECOND, 0);
	setupDisabilityOfInputElements();
    }

    /**
     * Called when the {@link DatePickerDialog} is finished which was created by
     * the {@link EditActivity#timePickerFragment}. So the method can store the
     * inputed values (year, month and day) in the
     * {@link EditActivity#remindCal} calendar.
     * 
     * @param view
     *            The view associated with this listener.
     * @param year
     *            The year that was set.
     * @param monthOfYear
     *            The month that was set (0-11) for compatibility with Calendar.
     * @param dayOfMonth
     *            The day of the month that was set.
     * 
     * @see {@link OnDateSetListener#onDateSet(DatePicker, int, int, int)}
     **/
    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

	remindCal.set(year, month, dayOfMonth);
	setupDisabilityOfInputElements();
    }

    /**
     * Set the Layout {@code res/layout/edit_activity.xml} as well as the
     * instance variables. To the CheckBoxes {@link EditActivity#done} and
     * {@link EditActivity#remind} the {@link OnCheckListener} is added as
     * Listener as well as to the two buttons ( {@link EditActivity#dateSetup},
     * {@link EditActivity#timeSetup}) are two anonymous {@link OnClickListener}
     * added which will show the corresponding dialog (
     * {@link EditActivity#timePickerFragment},
     * {@link EditActivity#datePickerFragment}). If the intent contains the
     * extra {@link MainActivity#REQUEST_CODE} for updating a item (
     * {@link MainActivity#ITEM_UPDATE}), this item will be fetched from the
     * database by calling the method
     * {@link DatabaseAdapter#fetchToDoItemWith(int, Context)} and the received
     * values of the ToDo item will be set to the corresponding fields.
     * 
     * @param savedInstanceState
     *            {@code null} or the {@link Bundle} contains the data it most
     *            recently supplied in
     *            {@link Activity#onSaveInstanceState(Bundle)} .
     * 
     * @see {@link Activity#onCreate(Bundle)}, {@link DatabaseAdapter},
     *      {@link MainActivity#REQUEST_CODE}
     **/
    @Override
    protected void onCreate(final Bundle savedInstanceState) {

	super.onCreate(savedInstanceState);
	setContentView(R.layout.edit_activity);

	title = (EditText) findViewById(R.id.title);
	description = (EditText) findViewById(R.id.description);
	done = (CheckBox) findViewById(R.id.done);
	done.setOnCheckedChangeListener(new OnCheckListener());
	remind = (RadioButton) findViewById(R.id.remind);
	location = (RadioButton) findViewById(R.id.asklocation);

	remind.setOnCheckedChangeListener(new OnCheckListener());
	location.setOnCheckedChangeListener(new OnCheckListener());

	customPos = (Button) findViewById(R.id.setGPS);

	customPos.setOnClickListener(new OnClickListener() {

	    @Override
	    public void onClick(View v) {

		// TODO This would call the Location-Editing Fragment but even
		// after hours and hours of trying and getting this to work I
		// gave up on it. There is a FragmentActivityHandler activity
		// that was supposed to handle the DialogFragment (I didn't for
		// the love of god get it to work in this activity.

	    }
	});

	timeSetup = (Button) findViewById(R.id.timeSetup);
	timeSetup.setOnClickListener(new OnClickListener() {

	    /**
	     * If the user clicks the listened view, the dialog for editing the
	     * time is displayed.
	     * 
	     * @param v
	     *            The view that was clicked.
	     * 
	     * @see {@link OnClickListener#onClick(View)}
	     **/
	    @Override
	    public void onClick(View v) {

		timePickerFragment.show(getFragmentManager(), "timePicker");
	    }
	});
	dateSetup = (Button) findViewById(R.id.dateSetup);
	dateSetup.setOnClickListener(new OnClickListener() {

	    /**
	     * If the user clicks the listened view, the dialog for editing the
	     * date is displayed.
	     * 
	     * @param v
	     *            The view that was clicked.
	     * 
	     * @see {@link OnClickListener#onClick(View)}
	     **/
	    @Override
	    public void onClick(View v) {

		datePickerFragment.show(getFragmentManager(), "datePicker");
	    }
	});

	/**
	 * Added this to create and update results with a button without a back
	 * button. Just because it's pretty Uses onBackPressed() method from
	 * further down the class
	 * 
	 */
	Button submit = (Button) findViewById(R.id.submitEvent);
	submit.setOnClickListener(new OnClickListener() {

	    @Override
	    public void onClick(View v) {

		onBackPressed();
	    }

	});

	remindInformation = (TextView) findViewById(R.id.dateTime_information);
	gpsInformation = (TextView) findViewById(R.id.gps_information);
	Intent intent = getIntent();
	try {
	    if (intent.getIntExtra(MainActivity.REQUEST_CODE, 0) == MainActivity.ITEM_UPDATE) {
		id = intent.getIntExtra(ToDoItem.ID, -1);
		if (id != -1) {
		    ToDoItem todo = DatabaseAdapter.fetchToDoItemWith(id, this);
		    title.setText(todo.getTitle());
		    description.setText(todo.getDescription());
		    done.setChecked(todo.isDone());
		    remind.setChecked(todo.shouldRemind());
		    remindCal.setTimeInMillis(todo.getRemindTimeInMillis());
		}
		setupDisabilityOfInputElements();
	    } else {
		setupDisabilityOfInputElements();
	    }
	} catch (Exception e) {
	    setupDisabilityOfInputElements();
	}
    } // On create end

    /**
     * Method called if the user presses the back button. The method will store
     * the inputed values in the database. If the processed ToDo item is a new
     * one ({@link EditActivity#id} = -1) the database is called to create a new
     * entry (
     * {@link DatabaseAdapter#createToDoItem(String, String, boolean, boolean, long, android.content.Context)}
     * ) otherwise the item will be updated in the database (
     * {@link DatabaseAdapter#updateToDoItemWith(int, String, String, boolean, boolean, long, android.content.Context)}
     * ). Before the {@code super.onBackPressed()} is called the result value is
     * set to {@link Activity#RESULT_OK} that this activity will return to its
     * caller {@link MainActivity} including a intent which contains the
     * identifier of the processed ToDo item.
     * 
     * @see {@link Activity#onBackPressed()}, {@link DatabaseAdapter}
     **/

    @Override
    public void onBackPressed() {

	String title = this.title.getText().toString();
	String description = this.description.getText().toString();
	boolean done = this.done.isChecked();
	boolean remind = this.remind.isChecked();
	long date = remindCal.getTimeInMillis();
	if (id == -1) {
	    id = DatabaseAdapter.createToDoItem(title, description, done,
		    remind, date, this);
	    if (id == -1) {
		setResult(RESULT_CANCELED);
		super.onBackPressed();
	    }
	} else {
	    DatabaseAdapter.updateToDoItemWith(id, title, description, done,
		    remind, date, this);
	}
	Intent intent = new Intent();
	intent.putExtra(ToDoItem.ID, id);
	setResult(RESULT_OK, intent);
	super.onBackPressed();
    }

    /**
     * Handles the selection in the options menu. In this activity two items are
     * selectable: firstly the possibility for canceling the editing so that the
     * application returns without storing the changed data to the
     * {@link MainActivity}. Therefore the {@link Activity#onBackPressed()} is
     * called and the result value was set to {@link MainActivity#RESULT_DELETE}
     * . Secondly the user can also select the option to delete this item from
     * the database which is done by the method
     * {@link DatabaseAdapter#deleteToDoItemWith(int, android.content.Context)}.
     * 
     * @param item
     *            The menu item that was selected.
     * @return Return {@code false} to allow normal menu processing to proceed,
     *         {@code true} to consume it here.
     * 
     * @see {@link Activity#onOptionsItemSelected(MenuItem)}
     **/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

	switch (item.getItemId()) {
	case R.id.menu_delete:
	    if (id != -1) {
		DatabaseAdapter.deleteToDoItemWith(id, this);
	    }
	    Intent intent = new Intent();
	    intent.putExtra(ToDoItem.ID, id);
	    setResult(MainActivity.RESULT_DELETE, intent);
	    super.onBackPressed();
	    break;
	case R.id.menu_cancel:
	    setResult(RESULT_CANCELED);
	    super.onBackPressed();
	}
	return true;
    }

    /**
     * Creates the options menu through the menu resource
     * {@code R.menu.edit_activity}. The menu will contains two items: firstly
     * for canceling of the editing an secondly an item initiate the deletion of
     * the edited ToDo item.
     * 
     * @param menu
     *            The options menu in which you place your items.
     * @result Return always {@code true} so that the menu will be displayed.
     * 
     * @see {@link Activity#onCreateOptionsMenu(Menu)}
     **/
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

	getMenuInflater().inflate(R.menu.edit_activity, menu);
	return true;
    }

    /**
     * Enables or disables the input elements of the done and the remind status
     * as well as the buttons to change the date & time depending on their
     * values ({@link EditActivity#done}, {@link EditActivity#remind},
     * {@link EditActivity#dateSetup}, {@link EditActivity#timeSetup}).
     * Following possibilities:
     * <ul>
     * <li>Done status is {@code true}: all other input elements are disabled
     * because the application will not remind for a done item.</li>
     * <li>Done and remind status is {@code false}: all input elements are
     * enabled except the buttons, since it is not useful to set a date for a
     * item where the remind function is disabled.</li>
     * <li>Done status is {@code false}, remind status is {@code true} : all
     * input element are enabled because remind function is activated.</li>
     * </ul>
     * For each case also an information message is shown in the
     * {@link EditActivity#remindInformation}.
     **/
    private void setupDisabilityOfInputElements() {

	boolean doneStatus = done.isChecked();
	boolean remindStatus = remind.isChecked();
	boolean locationStatus = location.isChecked();
	if (!locationStatus && remindStatus) {

	    // Time UI
	    remindInformation.setText(getString(R.string.reminder_information,
		    remindCal.getTime()));
	    location.setEnabled(true);
	    dateSetup.setEnabled(true);
	    timeSetup.setEnabled(true);

	    // Location UI
	    gpsInformation.setText("Location Disabled");
	    location.setEnabled(true);
	    customPos.setEnabled(false);
	} else if (locationStatus && !remindStatus) {

	    // Location UI
	    gpsInformation.setText("GPS Info here");
	    location.setEnabled(true);
	    customPos.setEnabled(true);

	    // Time UI
	    remindInformation.setText(getString(R.string.reminder_disabled));
	    remind.setEnabled(true);
	    dateSetup.setEnabled(false);
	    timeSetup.setEnabled(false);
	} else if (!locationStatus && !remindStatus && !doneStatus) {

	    // Location UI
	    gpsInformation.setText("Location Disabled");
	    location.setEnabled(true);
	    customPos.setEnabled(false);

	    // Time UI
	    remindInformation.setText(getString(R.string.reminder_disabled));
	    remind.setEnabled(true);
	    dateSetup.setEnabled(false);
	    timeSetup.setEnabled(false);

	} else if (doneStatus) {
	    gpsInformation.setText("Location Reached...sort of");
	    remindInformation.setText(getString(R.string.reminder_done));
	    customPos.setEnabled(false);
	    dateSetup.setEnabled(false);
	    timeSetup.setEnabled(false);
	}
    }
}