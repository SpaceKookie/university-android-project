package androidlab.exercise3_1.widget;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import androidlab.exercise3_1.*;
import androidlab.exercise3_1.adapter.*;
import androidlab.exercise3_1.model.*;

/**
 * <h2>BraodcastReceiver which updates the widgets</h2> This Class provides the
 * functionality to refresh all the widgets of this application. Therefore it
 * fetches the next ToDo item for which the application should remind and
 * updates the corresponding text fields in in the layout
 * {@code R.layout.widget}. If the application should not remind for any ToDo
 * item, this information will also be shown. This receiver receives
 * periodically an intent from an {@link AlarmManager} in the class
 * {@link Widget}.
 * 
 * @see {@link ToDoItem}, {@link DatabaseAdapter}, {@link Widget}
 * @author Seminar 'Introduction to Android Smartphone Programming', University
 *         of Freiburg
 * @version 1.3
 **/
public class WidgetUpdate extends BroadcastReceiver {

	/** Intent action of the intents which this Receiver receives */
	public static final String RECEIVER = "androidlab.exercise3_1.widget.update";

	/**
	 * Called when this {@link BroadcastReceiver} receives an intent. Expected
	 * is that these intents are send periodically from the AlarmManager in the
	 * {@link Widget} class. For each existing widget the shown information in
	 * this widget will be updated. More precisely, the ToDo item which remind
	 * occurrence is the next will be fetched from the database by calling
	 * {@link DatabaseAdapter#getUpcomingToDoItem(Context)}. If there exist such
	 * an item its information are set to the widget fields otherwise the text
	 * field in the widget should show that there is no item.
	 * 
	 * @param context
	 *            The Context in which the receiver is running.
	 * @param intent
	 *            The Intent being received.
	 * 
	 * @see {@link BroadcastReceiver#onReceive(Context, Intent)},
	 *      {@link DatabaseAdapter}, {@link Widget#onEnabled(Context)}
	 **/
	@Override
	public void onReceive(Context context, Intent intent) {
		ToDoItem todo = DatabaseAdapter.getUpcomingToDoItem(context);
		ComponentName thisWidget = new ComponentName(context, Widget.class);
		AppWidgetManager appWidgetManager = AppWidgetManager
				.getInstance(context);
		for (int widgetId : appWidgetManager.getAppWidgetIds(thisWidget)) {
			RemoteViews updateViews = new RemoteViews(context.getPackageName(),
					R.layout.widget);
			updateViews.setTextViewText(R.id.widget_app,
					context.getString(R.string.app_name));
			Intent i = new Intent(context, MainActivity.class);
			PendingIntent pendingIntent = PendingIntent.getActivity(context, 0,
					i, PendingIntent.FLAG_UPDATE_CURRENT);
			updateViews.setOnClickPendingIntent(R.id.widget_app, pendingIntent);
			if (todo != null) {
				updateViews.setTextViewText(R.id.widget_title, todo.getTitle());
				updateViews.setTextViewText(R.id.widget_datetime,
						String.format("%1$tF %1$tR", todo.getDate().getTime()));
			} else {
				updateViews.setTextViewText(R.id.widget_title,
						context.getString(R.string.no_upcoming));
				updateViews.setTextViewText(R.id.widget_datetime, "");
			}
			appWidgetManager.updateAppWidget(widgetId, updateViews);
		}
	}
}
