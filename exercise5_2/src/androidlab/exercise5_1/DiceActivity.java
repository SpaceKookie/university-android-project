package androidlab.exercise5_1;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.MotionEvent;

public class DiceActivity extends Activity implements SensorEventListener {

  // The OpenGL View
  private GLSurfaceView glSurface;
  private OpenGLRenderer render;
  private MediaPlayer audio;
  private SensorManager sm;
  private Sensor s;

  /**
   * 
   * Creates the application, initializes the OpenGL surface and renderer and also creates the MedaPlayer object
   * 
   */
  @Override
  protected void onCreate(Bundle savedInstanceState) {

	super.onCreate(savedInstanceState);

	glSurface = new GLSurfaceView(this);
	glSurface.setRenderer(render = new OpenGLRenderer(getBaseContext()));
	audio = MediaPlayer.create(this, R.raw.sound);
	audio.setLooping(false);

	sm = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
	if (sm.getSensorList(Sensor.TYPE_ACCELEROMETER).size() != 0) {
	  s = sm.getSensorList(Sensor.TYPE_ACCELEROMETER).get(0);
	  sm.registerListener(this, s, SensorManager.SENSOR_DELAY_NORMAL);
	}
	setContentView(glSurface);
  }

  @Override
  public void onSensorChanged(SensorEvent e) {

	System.out.println(e.values[0] + "  " + e.values[2]);
	// Further "fine tuning" would probably be required but I went with some values that seemed to be working quite well.
	if (e.values[0] > 8 || e.values[0] < -8) {
	  if (e.values[2] > 9 || e.values[2] < -9) {

		makeMagicHappen();
	  }
	}
  }

  /**
   * So I don't have to type it in two different methods
   */
  private void makeMagicHappen() {

	if (OpenGLRenderer.rotatingOfDOOMFire) {
	  audio.start();
	}
	render.touchy();
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {

	return false;
  }

  /**
   * Registering OpenGL Surface and accelerometer receiver
   */
  @Override
  protected void onResume() {

	super.onResume();
	glSurface.onResume();
	sm.registerListener(this, s, SensorManager.SENSOR_DELAY_NORMAL);
  }

  /**
   * Unregisteres the OpenGL Surface and accelerometer
   */
  @Override
  protected void onPause() {

	super.onPause();
	glSurface.onPause();
	sm.unregisterListener(this);
  }

  // Will register Touch Events. Only "UP" events (press-releases) will be used though
  @Override
  public boolean dispatchTouchEvent(MotionEvent event) {

	super.dispatchTouchEvent(event);
	if (event.getAction() == MotionEvent.ACTION_UP) {

	  // If the cube is rotating. Static reference to the rotating boolean from the Renderer class
	  makeMagicHappen();
	}

	return true;
  }

  @Override
  public void onAccuracyChanged(Sensor sensor, int accuracy) {

  }

}
