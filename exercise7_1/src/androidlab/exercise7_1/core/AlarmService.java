package androidlab.exercise7_1.core;

import android.app.IntentService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import androidlab.exercise7_1.panic.PanicState;
import androidlab.exercise7_1.utility.AppSettingsHelper;

/* 
 * Copyright (c) 2013 Katharina Sabel
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Background service to listen to USB-Cable removed events and also trigger an alarm. Decided to go for the
 * Intentservice as it is way easier to use and I want to be able to have different services running in the background,
 * depending on what the user wants to have enabled. This service handles the Loud (aka SOUND) Alarm.
 * 
 * @author Katharina
 * 
 */
public class AlarmService extends IntentService {

	private Context context;
	private IntentFilter filter;

	public AlarmService() {
		super(AlarmService.class.getName());

	}

	@Override
	protected void onHandleIntent(Intent intent) {
		context = getApplication();
		boolean running = true;
		System.out.println("Service started");
		filter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
		context.registerReceiver(receiver, filter);

		while (running) {

			boolean active = AppSettingsHelper.getApplicationState(context);
			boolean alarm = AppSettingsHelper.getLoudAlarmFlag(context);

			if (!active || !alarm) {
				if (receiver != null) {
					context.unregisterReceiver(receiver);
				}
				System.out.println("Service stopped");
				running = false;
				stopSelf();
			}
		}
	}

	public BroadcastReceiver receiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {

			int chargePlug = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);

			if (chargePlug == 0) {

				Intent startAlarm = new Intent(getApplicationContext(), GuardActivity.class);
				startAlarm.setAction(AppSettingsHelper.ALARM_INTENT_FILTER);
				startAlarm.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startAlarm.putExtra(AppSettingsHelper.TRIGGER_ALARM, true);
				context.sendBroadcast(startAlarm);

			} else if (chargePlug != 0) {

				Intent stopAlarm = new Intent(getApplicationContext(), GuardActivity.class);
				stopAlarm.setAction(AppSettingsHelper.ALARM_INTENT_FILTER);
				stopAlarm.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				stopAlarm.putExtra(AppSettingsHelper.TRIGGER_ALARM, false);
				context.sendBroadcast(stopAlarm);
			}
		}
	};
}
