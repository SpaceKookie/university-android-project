package androidlab.exercise6_1.core;

import java.util.ArrayList;
import java.util.Date;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.provider.CalendarContract.Events;
import android.telephony.PhoneStateListener;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;
import androidlab.exercise6_1.R;
import androidlab.exercise6_1.calls.Call;
import androidlab.exercise6_1.data.AppSettingsHelper;
import androidlab.exercise6_1.data.CalendarDealer;
import androidlab.exercise6_1.data.Event;
import androidlab.exercise6_1.data.EvilMananger;

/**
 * Broadcast Receiver to register incoming phonecalls, mute the phone if needed and also send out answer sms messages to the caller. Sort of like a
 * multi-talent. TODO: Fix the bug where this receiver will be unregistered when killing the notification service!
 * 
 * @author Konstantin
 * 
 */
public class CallMeMaybe extends BroadcastReceiver {

	private TelephonyManager man;
	private SmsManager msgMan;
	private CalendarDealer dealer;

	@Override
	public void onReceive(final Context context, Intent intent) {

		Log.i("call911", "Receiver registered");

		msgMan = android.telephony.SmsManager.getDefault();

		if (man == null) {
			man = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

			man.listen(new PhoneStateListener() {

				@Override
				public void onCallStateChanged(int state, String incomingNumber) {

					// Checking if the state is ringing. Otherwise this method will
					// be called again when rejecting the call and when idling
					// again,
					// creating new entries in the database, which I don't want.
					if (state == TelephonyManager.CALL_STATE_RINGING) {

						String mode = "Rang";

						if (AppSettingsHelper.getAppEnabledFlag(context)) {
							if (AppSettingsHelper.getCallBlockMode(context) == 1) {
								AudioManager audio = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
								audio.setRingerMode(AudioManager.RINGER_MODE_SILENT);

								mode = "Muted";

								if (AppSettingsHelper.getSMSEnabledFlag(context)) {

									if (incomingNumber != null) {
										msgMan.sendTextMessage(incomingNumber, null, context.getString(R.string.currently_busy), null, null);
									}
								}
							}

							// Called if the user set the mode to only block calls
							// when in meetings etc.
							if (AppSettingsHelper.getCallBlockMode(context) == 2) {

								int statusCheck;
								dealer = new CalendarDealer(context);
								ArrayList<Event> events = dealer.getEvents(System.currentTimeMillis());

								for (int n = 0; n < events.size(); n++) {
									statusCheck = events.get(n).getStatus();

									if (statusCheck == Events.AVAILABILITY_BUSY) {
										AudioManager audio = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
										audio.setRingerMode(AudioManager.RINGER_MODE_SILENT);
										break;

									}

								}

							} else {
								Toast.makeText(context, "Pick a Call-Block-Mode next time", Toast.LENGTH_LONG).show();
							}

							@SuppressWarnings("deprecation")
							String date = new Date(java.lang.System.currentTimeMillis())
							// I don't care that it's deprecated. It's an awesome
							// method and I want to use it :)
									.toLocaleString();

							Call call = new Call(numberCheck(incomingNumber), date, mode);

							EvilMananger.getInstance().getDatabase().addCallInfo(call);

							Intent intent = new Intent();
							intent.setAction("MyLittlePhony.Trigger.Rebuild");
							intent.putExtra("intentTag", "rebuild");
							Log.i("data", "Sending Intent");
							context.sendBroadcast(intent);

						}
					}
				}
			}, PhoneStateListener.LISTEN_CALL_STATE);
		}
	}

	/**
	 * Called to check if there is a phone number known from the caller. Trying to message a "null" number will result in a crashing application
	 * 
	 * @param incomingNumber
	 * @return
	 */
	private String numberCheck(String incomingNumber) {

		if (incomingNumber == null) {
			return "Private";
		} else {
			return incomingNumber;
		}
	}

}
