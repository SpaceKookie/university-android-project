package androidlab.exercise1_2;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.widget.TextView; //Wird f�r XML Layout nicht gebraucht

public class Main extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		/**
		 * Uncomment f�r XML-Layout
		 */
		// setContentView(R.layout.activity_main);

		/**
		 * Marker I
		 */

		/**
		 * Erste Aufgabe: Mit dem unten gezeigten Code den Inhalt der App
		 * erzeugen (In diesem Fall ein TextView Objekt Was eine simple
		 * Textausgabe auf dem Bildschirm erzeugt. Zum Testen den unteren Teil
		 * "un-commenten"
		 */

		// TextView tv = new TextView(this);
		// tv.setText(" Chuck Norris runs Android on his iPhone");
		// setContentView(tv);

		/**
		 * Marker II
		 */

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
}
