package androidlab.exercise7_1.fragments.full;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import androidlab.exercise7_1.R;
import androidlab.exercise7_1.core.GuardActivity;
import androidlab.exercise7_1.utility.AppSettingsHelper;
import androidlab.exercise7_1.utility.RenderStatus;

/* 
 * Copyright (c) 2013 Katharina Sabel
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Little overview screen to show if the application is running and also grant the user access to the settings menu
 * 
 * @author Katharina
 * 
 */
public class OverviewFragment extends Fragment {

	private View view;
	private Context context;
	private ImageView guardianEnabled, guardianDisabled, gear;
	private TextView thisWillChange;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle states) {
		view = inflater.inflate(R.layout.layout_overview_fragment, container, false);
		this.context = getActivity();

		buildUI(RenderStatus.RENDER_EVERYTHING);

		guardianDisabled.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				System.out.println("Click Click 2222");

				boolean currentState = AppSettingsHelper.getApplicationState(context);

				AppSettingsHelper.setApplicationState(!currentState, context);
				buildUI(RenderStatus.REFRESH_ACTIVITY_ICON);
			}
		});

		guardianEnabled.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				System.out.println("Click Click");
				boolean currentState = AppSettingsHelper.getApplicationState(context);

				AppSettingsHelper.setApplicationState(!currentState, context);
				buildUI(RenderStatus.REFRESH_ACTIVITY_ICON);
			}
		});

		gear.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				FragmentTransaction trans = getFragmentManager().beginTransaction();
				trans.replace(R.id.master_container, new SettingsFragment());
				trans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
				trans.addToBackStack("IN_APPLICATION");
				trans.commit();
			}
		});

		return view;
	}

	@Override
	public void onResume() {
		super.onResume();
		buildUI(RenderStatus.REFRESH_ACTIVITY_ICON);
	}

	private void buildUI(int status) {

		if (status == RenderStatus.RENDER_EVERYTHING) {
			guardianDisabled = (ImageView) view.findViewById(R.id.disabled_icon);
			guardianEnabled = (ImageView) view.findViewById(R.id.enabled_icon);
			gear = (ImageView) view.findViewById(R.id.settings_icon);
			thisWillChange = (TextView) view.findViewById(R.id.this_will_change_sometimes);

		} else if (status == RenderStatus.OVERVIEW_APP_STATUS_ENABLED) {
			guardianEnabled = (ImageView) view.findViewById(R.id.enabled_icon);
		} else if (status == RenderStatus.OVERVIEW_APP_STATUS_DISABLED) {
			guardianDisabled = (ImageView) view.findViewById(R.id.disabled_icon);
		} else if (status == RenderStatus.OVERVIEW_SETTINGS) {
			gear = (ImageView) view.findViewById(R.id.settings_icon);
		} else if (status == RenderStatus.REFRESH_ACTIVITY_ICON)

			if (AppSettingsHelper.getApplicationState(context)) {
				thisWillChange.setText(context.getString(R.string.enabled_text_info_on));
				guardianDisabled.setVisibility(View.GONE);
				guardianEnabled.setVisibility(View.VISIBLE);
			} else {
				thisWillChange.setText(context.getString(R.string.enabled_text_info_off));
				guardianDisabled.setVisibility(View.VISIBLE);
				guardianEnabled.setVisibility(View.GONE);
			}

	}

}
