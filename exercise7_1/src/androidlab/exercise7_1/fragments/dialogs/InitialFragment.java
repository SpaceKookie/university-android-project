package androidlab.exercise7_1.fragments.dialogs;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import androidlab.exercise7_1.R;

/* 
 * Copyright (c) 2013 Katharina Sabel
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This fragment will be displayed if the user opens the app for the first time
 * 
 * @author Katharina
 * 
 */
public class InitialFragment extends DialogFragment {

  private Button next;
  private Dialog d;

  /**
   * Will create a new instance of the @InitialFragment
   * 
   * @return An instance of the InitialFragment to be shown and worked with on the screen
   */
  public static InitialFragment newInstance() {
	InitialFragment frag = new InitialFragment();

	return frag;
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle states) {
	View v = inflater.inflate(R.layout.layout_setup_fragment, container, false);
	d = this.getDialog();

	d.setTitle(R.string.setup_fragment_title);

	// TODO: Fix this
	// d.setCancelable(false);
	d.setCanceledOnTouchOutside(false);

	next = (Button) v.findViewById(R.id.setup_fragment_first_continue);
	next.setOnClickListener(new OnClickListener() {

	  @Override
	  public void onClick(View v) {
		FragmentTransaction trans = getFragmentManager().beginTransaction();
		DialogFragment frag = PinSetFragment.newInstance();
		InitialFragment.this.dismiss();
		frag.show(trans, "Set Pin");

	  }
	});

	return v;
  }
}
