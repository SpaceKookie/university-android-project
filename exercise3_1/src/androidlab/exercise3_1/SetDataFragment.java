package androidlab.exercise3_1;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

/**
 * About the only class in the App that works flawlessly ;) Responsable for creating new events, collecting all the data, opening pop-up
 * fragments (Dialog Fragments) and inserting all that data into the database (well actually calling a method that does that)
 * 
 * @author Konstantin
 * 
 */
public class SetDataFragment extends Fragment {

  EditText name;
  TextView date;
  TextView time;
  EditText notes;
  CheckBox mlp;
  FragmentManager fragDealer;
  FragmentTransaction frtr;
  Fragment getData;
  Fragment setData;
  IntentFilter filter = new IntentFilter("LowOrbitIonCannon");
  private final String TAG = "SetDataScreen";

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle svaedInstanceState) {

	getActivity().registerReceiver(sateliteDish, filter);

	// Creating fragments
	fragDealer = getFragmentManager();
	frtr = fragDealer.beginTransaction();
	getData = new GetDataFragment();
	setData = new SetDataFragment();

	View view = inflater.inflate(R.layout.event_make, container, false);

	// Initializing the View
	name = (EditText) view.findViewById(R.id.setName);
	time = (TextView) view.findViewById(R.id.setTime);
	date = (TextView) view.findViewById(R.id.setDate);
	notes = (EditText) view.findViewById(R.id.setNotes);
	mlp = (CheckBox) view.findViewById(R.id.checkMLP);

	/**
	 * Clears the input
	 */
	Button clear = (Button) view.findViewById(R.id.mopTheFloor);
	clear.setOnClickListener(new OnClickListener() {

	  @Override
	  public void onClick(View v) {

		final String blank = "";
		name.setText(blank);
		date.setText(blank);
		time.setText(blank);
		notes.setText(blank);
		mlp.setChecked(false);

	  }
	});

	/**
	 * Handles the data input and writes data input to Database
	 */
	Button commit = (Button) view.findViewById(R.id.addEvent);
	commit.setOnClickListener(new OnClickListener() {

	  @Override
	  public void onClick(View v) {

		getActivity().unregisterReceiver(sateliteDish);
		DataHandler dealer = new DataHandler(getActivity());

		String nameData, noteData, timeData, dateData;
		boolean tickData;

		nameData = name.getText().toString();
		noteData = notes.getText().toString();
		timeData = time.getText().toString();
		dateData = date.getText().toString().replace(".", "-"); // Replacing points with dashes so the DateConverter can do it's magic :)
		tickData = mlp.isChecked();

		// Wrapping up the reminder with all the data to be inserted into the database
		Reminder reminder = new Reminder(nameData, false, tickData, OmniTools.DateConverter(dateData), OmniTools.TimeConverter(timeData),
			noteData);

		dealer.insertReminder(reminder);

		// Swapping Fragments to get back to the overview screen
		frtr.replace(R.id.frag_frame, getData, "GET");
		frtr.addToBackStack(null);
		frtr.commit();

	  }
	});

	/**
	 * Triggers the Calendar-picker-overlay-thingy
	 */
	TextView dateOverLay = (TextView) view.findViewById(R.id.setDate);
	dateOverLay.setOnClickListener(new OnClickListener() {

	  @Override
	  public void onClick(View v) {

		DialogFragment timePicker = new DatePicker();
		if (!timePicker.isVisible()) {
		  timePicker.show(fragDealer, "datePicker");
		}
		else {
		}

	  }
	});

	/**
	 * Same thing as above just for the time.
	 */
	TextView timeOverLay = (TextView) view.findViewById(R.id.setTime);
	timeOverLay.setOnClickListener(new OnClickListener() {

	  @Override
	  public void onClick(View v) {

		DialogFragment datePicker = new TimePicker();

		if (!datePicker.isVisible()) {
		  datePicker.show(fragDealer, "timePicker");
		}
		else {
		}

	  }
	});

	return view;
  }

  /**
   * Receives time and date data in form of intents
   * 
   * @param data
   * @throws Exception
   */
  private BroadcastReceiver sateliteDish = new BroadcastReceiver() {

	@Override
	public void onReceive(Context context, Intent data) {

	  Bundle extras = data.getExtras();

	  if (extras.get("type").equals("date")) {
		TextView date = (TextView) getView().findViewById(R.id.setDate);
		String tempDay = (String) extras.get("day");
		String tempMonth = (String) extras.get("month");
		String tempYear = (String) extras.get("year");
		date.setText(tempDay + "." + tempMonth + "." + tempYear);
	  }
	  else if (extras.get("type").equals("time")) {
		TextView time = (TextView) getView().findViewById(R.id.setTime);
		String tempMinute = (String) extras.get("minute");
		String tempHour = (String) extras.get("hour");
		time.setText(tempHour + ":" + tempMinute);

	  }
	  else {
		Log.e(TAG, "Bad intent");
	  }

	}
  };
}