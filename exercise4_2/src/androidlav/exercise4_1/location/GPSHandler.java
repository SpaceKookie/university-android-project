package androidlav.exercise4_1.location;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;

/**
 * Was supposed to handle GPS checking and input-output of location data
 * 
 * @author Konstantin
 * 
 */
public class GPSHandler extends Service {

    /**
     * Called by background service to check if a location has been reached yet.
     * Might add "fencing" around a target position by 10-15 metres
     * 
     * @param current
     * @param target
     * @return
     */

    @Override
    public void onCreate() {

    }

    public boolean areWeThereYet(Location current, Location target) {

	if (current.equals(target)) {
	    return true;
	} else {
	    return false;
	}
    }

    @Override
    public IBinder onBind(Intent intent) {
	return null;
    }

}
