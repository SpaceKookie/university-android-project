package androidlab.exercise3_1;

import java.util.ArrayList;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

/**
 * Fragment View for the output of Data
 * 
 * @author Konstantin
 * 
 */
public class GetDataFragment extends Fragment {

  // The worst possible way to implement it, yes I know. But I had already created a certain program structure and was lacking the time to
  // change it again :( Bad practice. And also bad time management
  static DataHandler dealer;
  private ListView list;
  private View view;

  IntentFilter filter = new IntentFilter("HighOrbitIonCannon");
  private Fragment setData = new SetDataFragment();

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle svaedInstanceState) {

	getActivity().registerReceiver(callMeMaybe, filter);
	dealer = new DataHandler(getActivity());
	view = inflater.inflate(R.layout.event_fetch, container, false);

	ArrayList<Reminder> reminders = dealer.throwReminder();
	ReminderAdapter adapter = new ReminderAdapter(view.getContext(), R.layout.util_data_row, reminders);
	list = (ListView) view.findViewById(R.id.remindList);
	list.setAdapter(adapter);

	// Button flush = (Button) view.findViewById(R.id.flush);
	// flush.setOnClickListener(new OnClickListener() {
	//
	// public void onClick(View v) {
	//
	// // TODO: If I had gotten the database update to work the code to delete all completed (marked) events would be here
	// }
	// });
	//
	// Button unCheck = (Button) view.findViewById(R.id.unCheckAll);
	// unCheck.setOnClickListener(new OnClickListener() {
	//
	// public void onClick(View v) {
	//
	// // TODO: If I had gotten the database update to work the code to unmark all marked Objects would be here. It would also set the
	// // COMPLETED flag in the database to 0. However, as I have not gotten this to work, the button remains dead (deleted it from the
	// layout as well
	//
	// }
	// });

	// Swaps the Fragment for the "make_event" layout. Much much faster than loading up a new activity
	Button create = (Button) view.findViewById(R.id.makeEvent);
	create.setOnClickListener(new OnClickListener() {

	  public void onClick(View v) {

		FragmentManager frmg = getFragmentManager();
		FragmentTransaction frtr = frmg.beginTransaction();
		frtr.replace(R.id.frag_frame, setData, "SET");
		frtr.addToBackStack(null);
		frtr.commit();

	  }
	});

	return view;
  }

  // Receives update-calls from the database to remake the ListView object. It gets called...maybe :)
  private BroadcastReceiver callMeMaybe = new BroadcastReceiver() {

	@Override
	public void onReceive(Context context, Intent data) {

	  // System.out.println("Intent received");
	  ArrayList<Reminder> stuff = dealer.throwReminder();
	  // for (int pos = 0; pos <= stuff.size(); pos++) {
	  // System.out.println(stuff.get(pos).isCompleted());
	  // }
	  ReminderAdapter adapter = new ReminderAdapter(view.getContext(), R.layout.util_data_row, stuff);
	  list.setAdapter(adapter);

	}
  };
}
