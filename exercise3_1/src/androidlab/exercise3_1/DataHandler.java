package androidlab.exercise3_1;

import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataHandler extends SQLiteOpenHelper {

  public static final String TABLE_NAME = "NEW_AND_IMPROVED";
  public static final String DATABASE_NAME = "NEW_AND_IMPROVED_BASE";
  public static final String C_ID = "_id";
  public static final String NAME = "name";
  public static final String COMPLETED = "done";
  public static final String NOTES = "notes";
  public static final String MLP = "mlp";
  public static final String DATE = "date";
  public static final String TIME = "time";
  public static final int VERSION = 2;

  Context context;

  /**
   * @return the context (to use for the intent)
   */
  public Context getContext() {

	return context;
  }

  /**
   * @param context
   *          the context to set
   */
  public void setContext(Context context) {

	this.context = context;
  }

  public DataHandler(Context context) {

	super(context, DATABASE_NAME, null, VERSION);
	this.setContext(context);
  }

  @Override
  public void onCreate(SQLiteDatabase db) {

	StringBuilder create = new StringBuilder();
	create.append("CREATE TABLE " + TABLE_NAME + "(");
	create.append(C_ID + " INTEGER PRIMARY KEY,");
	create.append(NAME + " TEXT,");
	create.append(COMPLETED + " INTEGER,");
	create.append(MLP + " INTEGER,");
	create.append(DATE + " TEXT,");
	create.append(TIME + " TEXT,");
	create.append(NOTES + " TEXT");
	create.append(")");
	db.execSQL(create.toString());
  }

  public void destroy() {

	SQLiteDatabase db = getWritableDatabase();
	db.delete(TABLE_NAME, null, null);
	db.close();
  }

  @Override
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

	// Drop older table
	db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);

	// Remake table
	onCreate(db);
  }

  // This works
  public void insertReminder(Reminder rem) {

	ContentValues cv = new ContentValues();
	cv.put(DataHandler.NAME, rem.getName());
	cv.put(DataHandler.COMPLETED, 1);
	cv.put(DataHandler.MLP, rem.isMlp());
	cv.put(DataHandler.TIME, rem.getTime().toString());
	cv.put(DataHandler.DATE, rem.getDate().toString());
	cv.put(DataHandler.NOTES, rem.getNotes());

	SQLiteDatabase db = getWritableDatabase();
	db.insert(TABLE_NAME, null, cv);
	db.close();

  }

  // This...is interesting. It shoudl work, there is no reason why it shouldn't. The right value makes it into the method but for some
  // reason maybe the saving process isn't executed properly. Not really sure what happens here. I hate SQL!
  public void updateReminder(Reminder rem) {

	System.out.println(rem.isCompleted());
	ContentValues cv = new ContentValues();

	cv.put(DataHandler.C_ID, rem.getID());
	cv.put(DataHandler.NAME, rem.getName());
	cv.put(COMPLETED, 1);
	cv.put(DataHandler.TIME, rem.getTime().toString());
	cv.put(DataHandler.DATE, rem.getDate().toString());
	cv.put(DataHandler.NOTES, rem.getNotes());
	cv.put(DataHandler.MLP, rem.isMlp());

	SQLiteDatabase db = getWritableDatabase();
	db.update(TABLE_NAME, cv, rem.getID() + " = ?", new String[] { String.valueOf(rem.getID()) });
	db.close();

	System.out.println("This is after it was inserted into the database " + findReminderWithID(rem.getID()).isMlp());

	Intent dataUpdate = new Intent("HighOrbitIonCannon");
	getContext().sendBroadcast(dataUpdate);

  }

  // This is used to populate the ListView. And it was supposed to be used to fetch new data from the Database (like after checking a
  // reminder as completed). But for some reason the COMPLETED field is the only one that gets ignored and stays on 0. Even when hardcoding
  // it. Not really sure why. Wasted about a day on this without success. Giving up now!
  public ArrayList<Reminder> throwReminder() {

	ArrayList<Reminder> result = new ArrayList<Reminder>();

	SQLiteDatabase db = getReadableDatabase();
	String[] column = { DataHandler.C_ID, DataHandler.NAME, DataHandler.COMPLETED, DataHandler.MLP, DataHandler.DATE, DataHandler.TIME,
		DataHandler.NOTES };
	Cursor cursor = db.query(TABLE_NAME, column, null, null, null, null, null);
	if (cursor != null) {

	  cursor.moveToFirst();
	  while (cursor.moveToNext()) {

		int _id = cursor.getInt(0);
		String name = cursor.getString(1);
		boolean completed = OmniTools.getBoolean(cursor.getInt(2));
		boolean mlp = OmniTools.getBoolean(cursor.getInt(3));
		Date date = Date.valueOf(cursor.getString(4));
		Time time = Time.valueOf(cursor.getString(5));
		String notes = cursor.getString(6);

		Reminder rem = new Reminder(_id, name, completed, mlp, date, time, notes);
		result.add(rem);
	  }
	}

	db.close();
	return result;

  }

  // Searches the Database for a Reminder by a specific ID
  public Reminder findReminderWithID(int ID) {

	SQLiteDatabase db = getReadableDatabase();

	String[] column = { DataHandler.C_ID, DataHandler.NAME, DataHandler.COMPLETED, DataHandler.MLP, DataHandler.DATE, DataHandler.TIME,
		DataHandler.NOTES };
	Cursor cursor = db.query(TABLE_NAME, column, C_ID + "=" + ID, null, null, null, null);
	cursor.moveToFirst();

	int _id = cursor.getInt(0);
	String name = cursor.getString(1);
	boolean completed = OmniTools.getBoolean(cursor.getInt(2));
	boolean mlp = OmniTools.getBoolean(cursor.getInt(3));
	Date date = Date.valueOf(cursor.getString(4));
	Time time = Time.valueOf(cursor.getString(5));
	String notes = cursor.getString(6);

	Reminder rem = new Reminder(_id, name, completed, mlp, date, time, notes);

	return rem;

  }
}
