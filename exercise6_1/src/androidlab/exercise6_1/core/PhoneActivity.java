package androidlab.exercise6_1.core;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.MenuItem;
import androidlab.exercise6_1.R;
import androidlab.exercise6_1.calls.Call;
import androidlab.exercise6_1.data.EvilMananger;
import androidlab.exercise6_1.data.SQLOfEvil;
import androidlab.exercise6_1.gui.PhoneFragment;

public class PhoneActivity extends FragmentActivity {

	// Declaring global variables everything nice and private
	private FragmentManager man;
	private FragmentTransaction trans;
	private Fragment overView;

	@Override
	protected void onCreate(Bundle states) {

		super.onCreate(states);
		setContentView(R.layout.splash_layout);

		// Creating singleton instance of the database
		EvilMananger j = EvilMananger.getInstance();
		SQLOfEvil evil = new SQLOfEvil(getApplicationContext());
		j.setEvil_table(evil);

		overView = new PhoneFragment();

		// Fragment Transaction to show PhoneFragment
		man = getSupportFragmentManager();
		trans = man.beginTransaction(); // No pun intended :P
		trans.replace(R.id.fragment_holder, overView, "OVERVIEW");
		trans.commit();
	}

	/**
	 * 
	 * @return ArrayList<Call> being used in the CallAdapter. I think :s
	 */
	public ArrayList<Call> getCallInformationArrayList() {

		return EvilMananger.getInstance().getDatabase().getCallInfo();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// TODO: Change menu to actionbar button?

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.splash_layout, menu);
		return true;
	}

	/**
	 * Starts the PreferencesFragment, built on the google Preferences API
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		// No checking for what menu item was pressed because there is only one
		// Should still check #badpractice
		Intent i = new Intent(this, SettingsActivity.class);
		startActivity(i);
		return true;
	}
}
