# Android Project Repository

In the winter semester 2012/13 I participated in an Android Application development project at my university. It was the first time I had to work with a large framework and vital for me to learn a lot of things about programming. Here is now my code for said project.

#### Note, that I'm no longer developing on these applications nor will I ever be!

I've now moved away from these and am currently working on other projects. Check my website www.katharinasabel.de for more infos.