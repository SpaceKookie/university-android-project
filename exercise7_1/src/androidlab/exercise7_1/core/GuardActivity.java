package androidlab.exercise7_1.core;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.Menu;
import androidlab.exercise7_1.R;
import androidlab.exercise7_1.fragments.full.LoginFragment;
import androidlab.exercise7_1.fragments.full.PanicFragment;
import androidlab.exercise7_1.panic.PanicState;
import androidlab.exercise7_1.utility.AppSettingsHelper;

/* 
 * Copyright (c) 2013 Katharina Sabel
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Main activity. Opened on App launch and also on App resume to prevent thieves to change the alarm settings while on
 * the run from an angry mob
 * 
 * @author Katharina
 * 
 */
public class GuardActivity extends Activity {

	private FragmentManager man;
	private FragmentTransaction trans;
	private LoginFragment loginFragment;
	private PanicFragment panicFragment;

	@Override
	protected void onCreate(Bundle states) {

		states = null;
		// Basic GUI initializing
		super.onCreate(states);
		new AppSettingsHelper(getApplication());
		new PanicState(getApplication());

		setContentView(R.layout.layout_initial_screen);

		loginFragment = new LoginFragment();
		panicFragment = new PanicFragment();
		man = getFragmentManager();

		resetGUI();
	}

	@Override
	public void onResume() {
		super.onResume();

		// This will make users land in the login screen again instead of the stored states
		resetGUI();
	}

	private void resetGUI() {

		// Will open the normal login fragment if everything is all-righty
		if (!AppSettingsHelper.getPanicFlag(getBaseContext())) {
			trans = man.beginTransaction();
			trans.replace(R.id.master_container, loginFragment, "LOGIN FRAGMENT");
			trans.commit();
		}
		// Will open the @PANIC fragment if the application is in panic mode
		else {
			trans = man.beginTransaction();
			trans.replace(R.id.master_container, panicFragment, "PANIC FRAGMENT");
			trans.commit();
		}

	}

	// Overriding this to ensure that the menu buttons aren't going to be shown
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return false;
	}

}
