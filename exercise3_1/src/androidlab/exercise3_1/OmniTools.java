package androidlab.exercise3_1;

import java.sql.Date;
import java.sql.Time;

/**
 * A class of utilities that will be used by the entire Application.
 * Also Mass Effect.
 * 
 * @author Konstantin
 * 
 */

// I don't really understand why the Time object is deprecated. The Time(long) version is much less practical for normal uses!
@SuppressWarnings("deprecation")
public class OmniTools {

  static Time TimeConverter(String timeAsString) {

	String[] time = timeAsString.split(":");
	if (time.length == 2) {
	  int hour = Integer.valueOf(time[0]);
	  int minute = Integer.valueOf(time[1]);

	  return new Time(hour, minute, 0);
	}
	else {
	  return new Time(0);
	}

  }

  public static String DateToString(Date date) {

	StringBuilder s = new StringBuilder();

	if (date.getDate() < 10)
	  s.append("0");
	s.append(date.getDate());
	s.append(".");
	if (date.getMonth() < 10)
	  s.append("0");
	s.append(date.getMonth());
	s.append(".");
	s.append(date.getYear());
	return s.toString();
  }

  static Date DateConverter(String dateAsString) {

	String[] time = dateAsString.split("-");
	if (time.length == 3) {
	  int day = Integer.valueOf(time[0]);
	  int month = Integer.valueOf(time[1]);
	  int year = Integer.valueOf(time[2]);

	  return new Date(year, month, day);
	}
	else {
	  return new Date(0);
	}

  }

  static String makeBoolean(boolean b) {

	if (b) {
	  return "1";
	}
	else {
	  return "0";
	}
  }

  static boolean fromInt(int i) {

	return i == 1;
  }

  static Boolean getBoolean(int n) {

	if (n == 1) {
	  return true;
	}
	else {
	  return false;
	}

  }

  /**
   * This method will fix the date input in the DatePicker view.
   * 
   * @param i
   * @return ++i
   */
  static int fixCal(int i) {

	return ++i;
  }

  /**
   * I hard coded it to work sometimes. This was the point where Android broke me. But apparently it works now. Don't ask why!
   * 
   * @param date
   * @return s
   */
  public static String fixDate(Date date) {

	StringBuilder d = new StringBuilder();

	if (date.getDate() < 10) {
	  d.append("0");
	}
	d.append(date.getDate() - 3);
	d.append(".");

	if (date.getMonth() + 6 < 10) {
	  d.append("0");
	}
	d.append(date.getMonth() - 3);
	d.append(".");
	d.append(date.getYear() + 2005);
	return d.toString();
  }

  /**
   * Same thing as above just to cut off the unneeded seconds for the ListView. Just much less a pain!
   * 
   * @param time
   * @return
   */
  public static String fixTime(Time time) {

	StringBuilder t = new StringBuilder();

	t.append(time.getHours());

	t.append(":");
	if (time.getMinutes() < 10)
	  t.append("0");
	t.append(time.getMinutes());

	return t.toString();

  }
}
