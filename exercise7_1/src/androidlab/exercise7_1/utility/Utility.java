package androidlab.exercise7_1.utility;

/* 
 * Copyright (c) 2013 Katharina Sabel
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Utility class to hold static convertion tools and flags and what not. You know my coding style by now, there will be
 * like one method in here :)
 * 
 * @author Katharina
 * 
 */
public class Utility {

  private static String HashKey;

  /**
   * Will take a user inputed PIN and convert it into a HashCode. This method is being called on PIN input as well as in
   * the database entry. The database will then check for the HashCode, not the raw String
   * 
   * @param StringKey
   *          User PIN as a raw String
   * 
   * @return The user inputed PIN as a HashCode encrypted String. Is not reverse-engineerable. Used to write and read
   *         from the database
   */
  public static String makeHashKey(String StringKey) {

	Integer temp = StringKey.hashCode();
	HashKey = (String) temp.toString();

	return HashKey;

  }

  public void soundLoader() {
	// TODO: Do something useful with this for the end release
  }

}
