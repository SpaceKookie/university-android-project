package androidlab.exercise3_1.model;

import java.util.Calendar;

import androidlab.exercise3_1.notification.*;

/**
 * <h2>Model of a ToDo item</h2> An instance of this class represents a single ToDo item. Such an item contains information about the title
 * {@link ToDoItem#title} and the description {@link ToDoItem#description} of the ToDo item, an unique identifier {@link ToDoItem#id}, the
 * information whether the item is done or not {@link ToDoItem#done}, and also the information whether the remind service
 * {@link RemindService} should remind the user ({@link ToDoItem#remind}). Therefore the class contains also the remind time
 * {@link ToDoItem#date} as a long value which are the milliseconds from Jan. 1, 1970 to the remind time.
 * <hr/>
 * The class also contains static identifier for all the properties of an ToDo item. This is for example important for the creation of the
 * database as well as for the access to the database (see {@link ToDoItem#ID}, {@link ToDoItem#TITLE}, {@link ToDoItem#DESCRIPTION},
 * {@link ToDoItem#DONE}, {@link ToDoItem#REMIND} and {@link ToDoItem#DATE}).
 * 
 * 
 * @author Seminar 'Introduction to Android Smartphone Programming', University of Freiburg
 * @version 1.3
 **/
public class ToDoItem {

  /** Identifier of the unique identifier of a ToDo item. */
  public static final String ID = "todo_id";
  /** Identifier of the ToDo item title. */
  public static final String TITLE = "todo_title";
  /** Identifier of the ToDo item description. */
  public static final String DESCRIPTION = "todo_description";
  /** Identifier of the information whether the ToDo item is done. */
  public static final String DONE = "todo_done";
  /**
   * Identifier of the information whether for the ToDo item should be remind.
   */
  public static final String REMIND = "todo_remind";
  /** Identifier of the information when the reminding occurs. */
  public static final String DATE = "todo_remind_date";
  public static final String GPSX = "todo_gps_x";
  public static final String GPSY = "todo_gps_y";

  /** Identifier of a ToDo item. */
  private int id;
  /** Title of a ToDo item. */
  private String title;
  /** Description of a ToDo item. */
  private String description;
  /** Status of the ToDo item, whether it is done or not. */
  private boolean done;
  /** Status, whether the ToDo item should remind. */
  private boolean remind;
  /**
   * Date & time of the reminding, more specifically long value which are the milliseconds from Jan. 1, 1970 to the remind time.
   **/
  private long date;

  /**
   * Constructor: This will create a new instance of a ToDo item including all required data, which is given to this instantiation.
   * 
   * @param id
   *          Identifier of the ToDo item.
   * @param title
   *          Title of the ToDo item.
   * @param description
   *          Description of the ToDo item
   * @param remind
   *          Should the application remind for this ToDo item?
   * @param done
   *          Is the ToDo item done?
   * @param date
   *          Date & time of the reminding (milliseconds from Jan. 1, 1970 to the remind occurrence)
   **/
  public ToDoItem(int id, String title, String description, boolean remind, boolean done, long date) {

	super();
	this.id = id;
	this.title = title;
	this.description = description;
	this.remind = remind;
	this.done = done;
	this.date = date;
  }

  /**
   * Returns the identifier of the ToDo item.
   * 
   * @return The id of this ToDo item.
   **/
  public int getId() {

	return id;
  }

  /**
   * Returns the title of the ToDo item.
   * 
   * @return THe title of this ToDo item.
   **/
  public String getTitle() {

	return title;
  }

  /**
   * Returns the description of the ToDo item.
   * 
   * @return The description of this ToDo item.
   **/
  public String getDescription() {

	return description;
  }

  /**
   * Returns the information, whether for this ToDo item the application should remind the user or not.
   * 
   * @return Should the application remind for this ToDo item?
   **/
  public boolean shouldRemind() {

	return remind;
  }

  /**
   * Return the information, whether the ToDo item is done or not.
   * 
   * @return Is the ToDo item done?
   **/
  public boolean isDone() {

	return done;
  }

  /**
   * Returns the remind date as Calendar instance.
   * 
   * @return The remind date of this ToDo item.
   **/
  public Calendar getDate() {

	Calendar cal = Calendar.getInstance();
	cal.setTimeInMillis(date);
	return cal;
  }

  /**
   * Checks whether the occurrence of the remind date is after the current date and time.
   * 
   * @return Is the remind date of this ToDo item after now?
   **/
  public boolean isAfterNow() {

	return Calendar.getInstance().before(getDate());
  }

  /**
   * Returns the milliseconds from Jan. 1, 1970 to the remind time.
   * 
   * @return The milliseconds when the application should remind to this ToDo item.
   **/
  public long getRemindTimeInMillis() {

	return date;
  }
}
