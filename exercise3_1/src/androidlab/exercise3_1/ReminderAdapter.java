package androidlab.exercise3_1;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import android.view.View.OnClickListener;

public class ReminderAdapter extends ArrayAdapter<Reminder> {

  Context context;
  int layoutResourceId;
  ArrayList<Reminder> reminders = null;

  public ReminderAdapter(Context context, int layoutResourceId, ArrayList<Reminder> data) {

	super(context, layoutResourceId, data);
	this.layoutResourceId = layoutResourceId;
	this.context = context;
	this.reminders = data;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {

	View row = convertView;
	ReminderHolder holder = null;

	// Creates the view by populating the holder object which will then be returned to the adapter using it in the GUI class
	if (row == null) {
	  LayoutInflater inflater = ((Activity) context).getLayoutInflater();
	  row = inflater.inflate(layoutResourceId, parent, false);

	  holder = new ReminderHolder();
	  holder.completed = (CheckBox) row.findViewById(R.id.completed);
	  holder.name = (TextView) row.findViewById(R.id.name);
	  holder.time = (TextView) row.findViewById(R.id.time);
	  holder.date = (TextView) row.findViewById(R.id.date);
	  holder.mlp = (TextView) row.findViewById(R.id.mlp);
	  holder.comment = (TextView) row.findViewById(R.id.comment);

	  row.setTag(holder);

	}
	else {
	  holder = (ReminderHolder) row.getTag();
	}

	final Reminder reminder = reminders.get(position);
	holder.comment.setText(reminders.get(position).getNotes());

	// For some reason this doesn't work. Don't ask why. I spent about 6 hours just trying to figure out why. I write a value into the
	// database but the value doesn't come out. Even if I hardcode the value to be "1" instead of "0" there is a 0. I really don't like SQL
	holder.completed.setTag(reminder.getID());
	row.setTag(reminder.getID());
	holder.completed.setChecked(reminder.isCompleted());

	if (reminder.isCompleted()) {
	  holder.comment.setEnabled(false);
	}

	holder.name.setText(reminders.get(position).getName());
	holder.time.setText(OmniTools.fixTime(reminders.get(position).getTime()));
	holder.date.setText(OmniTools.DateToString(reminders.get(position).getDate()));
	holder.mlp.setText(OmniTools.makeBoolean(reminders.get(position).isMlp()));

	holder.completed.setOnClickListener(new OnClickListener() {

	  @Override
	  public void onClick(View v) {

		CheckBox doneBox = (CheckBox) v;
		int reminderTag = (Integer) v.getTag();

		// I know I'm the worst
		Reminder rem = GetDataFragment.dealer.findReminderWithID(reminderTag);
		if (doneBox.isChecked() != rem.isCompleted()) {
		  rem.setCompleted(doneBox.isChecked());
		}
		GetDataFragment.dealer.updateReminder(rem);
	  }

	});

	row.setOnClickListener(new OnClickListener() {

	  @Override
	  public void onClick(View v) {

		// TODO: Code to trigger the edit window!

	  }
	});

	row.setTag(holder);
	return row;
  }

  static class ReminderHolder {

	// TODO: Switch TextView mlp to ImageView
	// This was supposed to display a clock to indicate that a reminder has been set.

	CheckBox completed;
	TextView name;
	TextView time;
	TextView date;
	TextView mlp;
	TextView comment;

  }

}
