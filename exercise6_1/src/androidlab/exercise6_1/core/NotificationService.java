package androidlab.exercise6_1.core;

import android.app.IntentService;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.telephony.TelephonyManager;
import android.util.Log;
import androidlab.exercise6_1.R;
import androidlab.exercise6_1.data.AppSettingsHelper;

/**
 * Background service to register and unregister broadcastreceivers and also display the appropriate notifications
 * 
 * @author Konstantin
 * 
 */
public class NotificationService extends IntentService {

	private IntentFilter filter;
	private boolean alreadyThere = false;
	public BroadcastReceiver receiver;
	private boolean running;

	private enum STATE {
		ACTIVE, SLEEPING
	}

	private STATE current;

	public NotificationService() {

		super(NotificationService.class.getName());

	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// Setting the Service-State to active because it was just triggered
		current = STATE.ACTIVE;
		running = true;

		// Will only register the broadcast receiver if it has not yet been registered
		if (!alreadyThere) {
			receiver = new CallMeMaybe();
			filter = new IntentFilter(TelephonyManager.ACTION_PHONE_STATE_CHANGED);
			getBaseContext().registerReceiver(receiver, filter);
			alreadyThere = true;
			Log.i("Debug", "RECEIVER REGISTERED");
		}

		// Does the user want a notification?
		if (AppSettingsHelper.getNotificationEnabledFlag(getBaseContext()) && AppSettingsHelper.getCenterEnabledFlag(getBaseContext())) {

			// Passing on true if the LED is supposed to be on
			if (AppSettingsHelper.getLEDEnabledFlag(getBaseContext())) {
				notifySomebodyElse(true);
			}
			// and false if it's not
			notifySomebodyElse(false);
		}

		while (running) {

			// TODO: Fix this bug: https://bitbucket.org/LeGemiNi/android-university-project/issue/1/killing-background-service
			// TODO: Fix LED display

			boolean app = AppSettingsHelper.getAppEnabledFlag(getBaseContext());
			boolean notification = AppSettingsHelper.getNotificationEnabledFlag(getBaseContext());
			boolean center = AppSettingsHelper.getCenterEnabledFlag(getBaseContext());
			boolean led = AppSettingsHelper.getLEDEnabledFlag(getBaseContext());

			if (!app || !notification || !center) {
				if (!led) {
					notifySomebodyElse(true);
				}
				if (alreadyThere) {

					Log.i("Debug", "RECEIVER UN - REGISTERED");
					getBaseContext().unregisterReceiver(receiver);
					alreadyThere = false;

					running = false;

					stopSelf();
				}
			}

		}
	}

	private void notifySomebodyElse(boolean led) {

		if (current == STATE.SLEEPING) {
			stopForeground(true);
			return;
		}
		String title = getString(R.string.notification_title_active);
		String msg = getString(R.string.notification_msg_active);
		Integer icon = R.drawable.ic_launcher;
		Intent intent = new Intent(this, PhoneActivity.class);
		PendingIntent pendingIntent = PendingIntent.getActivity(getApplication(), 0, intent, Intent.FLAG_ACTIVITY_CLEAR_TASK);

		if (!led) {

			Notification.Builder not = new Notification.Builder(getBaseContext());
			not.setSmallIcon(icon).setContentTitle(title).setContentText(msg).setContentIntent(pendingIntent);
			startForeground(Notification.FLAG_FOREGROUND_SERVICE, not.build());
		}

		// If the LED is suppsed to be on this will create the same notification as above but add the LED to the notification. Not sure this works though.
		// Theoretically it should, my cell (Samsung Galaxy S3) never LEDed up though :(
		else {

			Notification.Builder not = new Notification.Builder(getBaseContext());
			not.setPriority(Notification.PRIORITY_MAX).setSmallIcon(icon).setContentTitle(title).setContentText(msg).setContentIntent(pendingIntent)
					.setLights(0xff1493, 1000, 3000);
			startForeground(Notification.FLAG_FOREGROUND_SERVICE, not.build());
		}
	}

}
