package androidlab.exercise3_1;

import android.support.v4.app.DialogFragment;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;

import java.util.Calendar;

public class TimePicker extends DialogFragment implements TimePickerDialog.OnTimeSetListener {

  SetDataFragment sdf = new SetDataFragment();

  public Dialog onCreateDialog(Bundle savedInstanceState) {

	final Calendar cal = Calendar.getInstance();
	int hour = cal.get(Calendar.HOUR_OF_DAY);
	int minute = cal.get(Calendar.MINUTE);

	return new TimePickerDialog(getActivity(), this, hour, minute, true);
  }

  @Override
  public void onTimeSet(android.widget.TimePicker view, int hourOfDay, int minute) {

	String zero = "0";
	String hour = Integer.toString(hourOfDay);
	String min = Integer.toString(minute);

	if (hourOfDay < 10 || hourOfDay == 0) {
	  hour = zero.concat(Integer.toString(hourOfDay));
	}
	if (minute < 10 || minute == 0) {
	  min = zero.concat(Integer.toString(minute));
	}

	// Sending the time Info to the SetDataFragment CLass.
	Intent timeTravel = new Intent("LowOrbitIonCannon");
	timeTravel.putExtra("type", "time");
	timeTravel.putExtra("hour", hour);
	timeTravel.putExtra("minute", min);

	getActivity().sendBroadcast(timeTravel);
  }
}
