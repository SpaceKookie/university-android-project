package androidlab.exercise7_1.fragments.dialogs;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidlab.exercise7_1.R;
import androidlab.exercise7_1.core.NumPadListener;
import androidlab.exercise7_1.fragments.full.LoginFragment;
import androidlab.exercise7_1.utility.AppSettingsHelper;
import androidlab.exercise7_1.utility.RenderStatus;
import androidlab.exercise7_1.utility.Utility;

/* 
 * Copyright (c) 2013 Katharina Sabel
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class PinSetFragment extends DialogFragment {

  private Dialog d;
  private View view;
  private Context context;
  private IntentFilter filter;
  private OnClickListener numpad;
  private String firstAttempt;
  private String secondAttempt;

  // ******* GUI ELEMENTS *******
  private Button one, two, three, four, five, six, seven, eight, nine, zero, submit, back;
  private EditText pinfield;

  /**
   * Will create a new instance of the @InitialFragment and add a Bundle with a number count to it
   * 
   * @return Instance of the PinSetFragment
   */
  static PinSetFragment newInstance() {
	PinSetFragment frag = new PinSetFragment();

	return frag;
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle states) {
	view = inflater.inflate(R.layout.layout_pin_reset, container, false);
	d = getDialog();
	context = getActivity();

	filter = new IntentFilter(AppSettingsHelper.NUMPAD_INTENT_FILTER);
	numpad = new NumPadListener(context);

	context.registerReceiver(PinReceiver, filter);

	d.setTitle(R.string.pin_fragment_title);
	d.setCanceledOnTouchOutside(false);

	buildUI(RenderStatus.RENDER_EVERYTHING);

	return view;
  }

  private BroadcastReceiver PinReceiver = new BroadcastReceiver() {

	@Override
	public void onReceive(Context context, Intent intent) {

	  System.out.println("Recieving");
	  System.out.println(intent.hasExtra("ButtonID"));
	  Bundle extras = intent.getExtras();
	  if (intent.hasExtra("ButtonID")) {
		int b = (Integer) extras.get("ButtonID");
		addToTextField(b);
	  }
	}
  };

  private void addToTextField(Integer b) {

	// Clear the textfield
	// TODO: Only remove one char from the field?
	if (b == -1) {
	  pinfield.setText(null);
	}
	else if (b == -2) {
	  callLogin();
	}
	else {
	  System.out.println();
	  String temp = pinfield.getText().toString();

	  // User @pin should only be 8 digits long
	  if (temp.length() < 8)
		pinfield.setText(temp.concat(b.toString()));
	}
	// TODO: Generate PIN reset key
  }

  /**
   * Slightly different than the @LoginFragment counterpart. This method will save the entered PIN in the @SharedPreferences Table. The PIN
   * will be stored as a @HashCode String.
   */
  private void callLogin() {

	if (firstAttempt == null) {
	  if (pinfield.getText().toString().equals("")) {
		Toast.makeText(context, "Enter a valid PIN", Toast.LENGTH_SHORT).show();
	  }
	  else {
		firstAttempt = pinfield.getText().toString();
		pinfield.setText(null);
		Toast.makeText(context, "Confirm your PIN", Toast.LENGTH_SHORT).show();
	  }
	}
	else {
	  secondAttempt = pinfield.getText().toString();
	  System.out.println(secondAttempt);
	  pinfield.setText(null);

	  if (firstAttempt.equals(secondAttempt)) {
		Toast.makeText(context, "Your PIN has been saved!", Toast.LENGTH_SHORT).show();
		AppSettingsHelper.setApplicationPin(Utility.makeHashKey(firstAttempt), context);
		pinfield.setText(null);

		FragmentTransaction trans = getFragmentManager().beginTransaction();
		trans.replace(R.id.master_container, new LoginFragment());
		trans.commit();
		context.unregisterReceiver(PinReceiver);
		d.dismiss();

	  }
	  else {
		Toast.makeText(context, "PINs don't match. Try again", Toast.LENGTH_SHORT).show();
		pinfield.setText(null);
		firstAttempt = secondAttempt = null;

	  }

	}
  }

  private void buildUI(int status) {

	if (status == RenderStatus.RENDER_EVERYTHING) {

	  pinfield = (EditText) view.findViewById(R.id.login_screen_pin2);

	  zero = (Button) view.findViewById(R.id.login_screen_button_zero2);
	  one = (Button) view.findViewById(R.id.login_screen_button_one2);
	  two = (Button) view.findViewById(R.id.login_screen_button_two2);
	  three = (Button) view.findViewById(R.id.login_screen_button_three2);
	  four = (Button) view.findViewById(R.id.login_screen_button_four2);
	  five = (Button) view.findViewById(R.id.login_screen_button_five2);
	  six = (Button) view.findViewById(R.id.login_screen_button_six2);
	  seven = (Button) view.findViewById(R.id.login_screen_button_seven2);
	  eight = (Button) view.findViewById(R.id.login_screen_button_eight2);
	  nine = (Button) view.findViewById(R.id.login_screen_button_nine2);
	  submit = (Button) view.findViewById(R.id.login_screen_button_submit2);
	  back = (Button) view.findViewById(R.id.login_screen_button_back2);

	  setOnClickListener(numpad);
	}

  }

  private void setOnClickListener(OnClickListener l) {
	zero.setOnClickListener(l);
	one.setOnClickListener(l);
	two.setOnClickListener(l);
	three.setOnClickListener(l);
	four.setOnClickListener(l);
	five.setOnClickListener(l);
	six.setOnClickListener(l);
	seven.setOnClickListener(l);
	eight.setOnClickListener(l);
	nine.setOnClickListener(l);
	submit.setOnClickListener(l);
	back.setOnClickListener(l);
  }

}
