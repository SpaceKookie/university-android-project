package androidlab.exercise5_1;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import android.opengl.GLSurfaceView.Renderer;
import android.opengl.GLU;
import android.util.Log;

class OpenGLRenderer implements Renderer {

  private CubicPolygon cube;
  private Rotation rot;

  // This is what happens if I let my brother have a look at my code for 2
  // seconds :)
  public static boolean rotatingOfDOOMFire;

  private boolean numberHasBeenDrawn;
  private boolean backwards;

  // Variables that control the cube rotation split in x, y and z axis.
  // Implementation of rotation functions is further down below
  private float xForce;
  private float counterForce;
  private float yForce;
  private float zForce;

  private int number;

  /**
   * @return the xRotating
   */

  private Context context;

  // Passing in the application context here so that it can be used to draw
  // the textures in the @CubicPolygon class
  public OpenGLRenderer(Context context) {

	// Reset rotation on launch
	counterForce = -0.1f;
	xForce = 3.5f;
	yForce = 1.5f;
	zForce = 2f;

	this.context = context;
	cube = new CubicPolygon();
	rot = new Rotation(0, 0, 0);

  }

  // Called when Touch Event occurs
  public void touchy() {

	if (!rotatingOfDOOMFire) {
	  numberHasBeenDrawn = false;
	  rotatingOfDOOMFire = true;
	}
	else if (rotatingOfDOOMFire) {
	  rotatingOfDOOMFire = false;
	  number = MakeItRandomBaby.random();
	  numberHasBeenDrawn = true;
	}

  }

  // Called if rotation is already enabled and another touch event hits
  private float xAnimation(int n) {

	switch (n) {
	case 1:
	  return 0f;

	case 2:
	  return 180f;

	case 3:
	  return 0f;

	case 4:
	  return 270f;

	case 5:
	  return 90f;

	case 6:
	  return 0f;

	default:
	  Log.i("ERROR", "Couldn't switch-case random number");
	  return -1;
	}

  }

  private float yAnimation(int n) {

	switch (n) {
	case 1:
	  return 270f;

	case 2:
	  return 0f;

	case 3:
	  return 90f;

	case 4:
	  return 0f;

	case 5:
	  return 0f;

	case 6:
	  return 0f;

	default:
	  Log.i("ERROR", "Couldn't switch-case random number");
	  return -1;
	}

  }

  public void onSurfaceCreated(GL10 gl, EGLConfig config) {

	cube.loadGLTexture(gl, this.context);

	gl.glEnable(GL10.GL_TEXTURE_2D);
	gl.glShadeModel(GL10.GL_SMOOTH);
	gl.glClearColor(0.5f, 0.5f, 0.5f, 0.3f);
	gl.glClearDepthf(1.0f);
	gl.glEnable(GL10.GL_DEPTH_TEST);
	gl.glDepthFunc(GL10.GL_LEQUAL);

	// Really Nice Perspective Calculations
	gl.glHint(GL10.GL_PERSPECTIVE_CORRECTION_HINT, GL10.GL_NICEST);
  }

  // Calculate x-rotating angle
  private float getXRotation(boolean doomFire) {

	if (doomFire) {
	  if (xForce > 360f) {
		backwards = true;
	  }
	  else if (xForce < -360f) {
		backwards = false;
	  }
	  if (!backwards) {
		if (xForce < 0) {
		  xForce -= counterForce;
		}
		rot.addX(2.5f);
		return xForce += 2.5f;
	  }
	  else if (backwards) {
		rot.addX(-2.5f);
		xForce += counterForce;
		return xForce -= 2.5f;
	  }
	}
	return 0;
  }

  private float getYRotation(boolean doomFire) {

	if (doomFire) {
	  if (yForce > 575f) {
		backwards = true;
	  }
	  else if (yForce < -575f) {
		backwards = false;
	  }
	  if (!backwards) {
		if (yForce < 0) {
		  yForce -= counterForce;
		}
		rot.addY(2.5f);
		return yForce += 2.5f;
	  }
	  else if (backwards) {
		rot.addY(-2.5f);
		yForce += counterForce;
		return yForce -= 2.5f;
	  }
	}
	return 0;

  }

  private float getZRotation(boolean doomFire) {

	if (doomFire) {
	  if (zForce > 855f) {
		backwards = true;
	  }
	  else if (zForce < -855f) {
		backwards = false;
	  }
	  if (!backwards) {
		if (zForce < 0) {
		  zForce -= counterForce;
		}
		rot.addZ(2.5f);
		return zForce += 2.5f;
	  }
	  else if (backwards) {
		rot.addZ(-2.5f);
		zForce += counterForce;
		return zForce -= 2.5f;
	  }
	}
	return 0;

  }

  public void onDrawFrame(GL10 gl) {

	gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
	gl.glLoadIdentity();

	gl.glTranslatef(0.0f, 0.0f, -9.0f);
	gl.glScalef(0.8f, 0.8f, 0.8f);

	// If it's just a regular rotation
	if (!numberHasBeenDrawn) {
	  gl.glRotatef(getXRotation(rotatingOfDOOMFire), 1, 0, 0);
	  gl.glRotatef(getYRotation(rotatingOfDOOMFire), 0, 1, 0);
	  gl.glRotatef(getZRotation(rotatingOfDOOMFire), 0, 0, 1);
	}

	// If there is a chosen one (number that is)
	if (numberHasBeenDrawn) {

	  /**
	   * Maybe a little bad practise but I really wanted to have the cube snap into position WITH the sound that's being played (the
	   * MediaPlayer needs about half a second to load
	   */
	  try {
		Thread.sleep(100);
	  }
	  catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	  }

	  gl.glRotatef(xAnimation(number), 1, 0, 0);
	  gl.glRotatef(yAnimation(number), 0, 1, 0);

	}

	gl.glFinish(); // Supposed to reduce render glitching. Don't think it did though :/
	cube.draw(gl);

  }

  /**
   * If the surface changes, reset the view
   */
  public void onSurfaceChanged(GL10 gl, int width, int height) {

	if (height == 0) { // Prevent A Divide By Zero By
	  height = 1; // Making Height Equal One
	}

	gl.glViewport(0, 0, width, height); // Reset The Current Viewport
	gl.glMatrixMode(GL10.GL_PROJECTION); // Select The Projection Matrix
	gl.glLoadIdentity(); // Reset The Projection Matrix

	// Calculate The Aspect Ratio Of The Window
	GLU.gluPerspective(gl, 45.0f, (float) width / (float) height, 0.1f, 100.0f);

	gl.glMatrixMode(GL10.GL_MODELVIEW); // Select The Modelview Matrix
	gl.glLoadIdentity(); // Reset The Modelview Matrix
  }
}