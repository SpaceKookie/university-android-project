package androidlab.exercise3_1.model;

import java.util.Comparator;

/**
 * <h2>Comparator for ToDo item</h2> Class which implements the
 * {@link Comparator} interface and because of that this class provides the
 * possibility to compare two ToDo items ({@link ToDoItem}).
 * 
 * @see {@link ToDoItem}
 * @author Seminar 'Introduction to Android Smartphone Programming', University
 *         of Freiburg
 * @version 1.3
 **/
public class ToDoComparator implements Comparator<ToDoItem> {

	/**
	 * Compares two ToDo items:
	 * <ol>
	 * <li>first criterion is the done status of the ToDo items</li>
	 * <li>second criterion is the remind status of the ToDo items</li>
	 * <li>third criterion is the remind date of the ToDo items</li>
	 * <li>fourth criterion is the title of the ToDo items</li>
	 * </ol>
	 * 
	 * @param lhs
	 *            first ToDo item which should be compared to {@code rhs}
	 * @param rhs
	 *            second ToDo item which should be compared to {@code lhs}
	 * @return In each criterion the method returns -1, if the {@code lhs} or 1,
	 *         if the {@code rhs} is higher in the order. If the items are equal
	 *         in one criterion then the next criterion will be compared.
	 * 
	 * @see {@link Comparator#compare(Object, Object)}
	 **/
	@Override
	public int compare(ToDoItem lhs, ToDoItem rhs) {
		if (lhs.isDone() && !rhs.isDone()) {
			return 1;
		} else if (!lhs.isDone() && rhs.isDone()) {
			return -1;
		} else {
			if (rhs.shouldRemind() && !lhs.shouldRemind()) {
				return 1;
			} else if (!rhs.shouldRemind() && lhs.shouldRemind()) {
				return -1;
			} else {
				int temp = lhs.getDate().compareTo(rhs.getDate());
				if (temp != 0) {
					return temp;
				} else {
					return lhs.getTitle().compareTo(rhs.getTitle());
				}
			}
		}
	}
}
