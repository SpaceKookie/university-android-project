package androidlab.exercise7_1.core;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import androidlab.exercise7_1.R;
import androidlab.exercise7_1.utility.AppSettingsHelper;

/* 
 * Copyright (c) 2013 Katharina Sabel
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This broadcast receiver is being used to distinguish button presses for the numpad. This is the case in the login
 * screen as well as the PanicStateScreen which activates if the phone is being stolen to deactivate the alarm again.
 * 
 * @author Katharina
 * 
 */
public class NumPadListener implements OnClickListener {

  private Context context;

  /**
   * Captures all numpad input and sends intents. TODO: Change this to something less pathetic :)
   * 
   * @param This
   *          will either be the @LoginFragment, @PinSetFragment or @PanicFragment context for the @intent
   */
  public NumPadListener(Context context) {
	this.context = context;
  }

  @Override
  public void onClick(View v) {

	Intent intent = new Intent();
	int b = v.getId();
	intent.setAction(AppSettingsHelper.NUMPAD_INTENT_FILTER);

	switch (b) {
	case R.id.login_screen_button_zero:
	  intent.putExtra("ButtonID", 0);
	  break;

	case R.id.login_screen_button_one:
	  intent.putExtra("ButtonID", 1);
	  break;

	case R.id.login_screen_button_two:
	  intent.putExtra("ButtonID", 2);
	  break;

	case R.id.login_screen_button_three:
	  intent.putExtra("ButtonID", 3);
	  break;

	case R.id.login_screen_button_four:
	  intent.putExtra("ButtonID", 4);
	  break;

	case R.id.login_screen_button_five:
	  intent.putExtra("ButtonID", 5);
	  break;

	case R.id.login_screen_button_six:
	  intent.putExtra("ButtonID", 6);
	  break;

	case R.id.login_screen_button_seven:
	  intent.putExtra("ButtonID", 7);
	  break;

	case R.id.login_screen_button_eight:
	  intent.putExtra("ButtonID", 8);
	  break;

	case R.id.login_screen_button_nine:
	  intent.putExtra("ButtonID", 9);
	  break;

	case R.id.login_screen_button_back:
	  intent.putExtra("ButtonID", -1);
	  break;

	case R.id.login_screen_button_submit:
	  intent.putExtra("ButtonID", -2);
	  break;

	case R.id.login_screen_button_forgot:
	  intent.putExtra("ButtonID", -3);
	  break;

	/**
	 * SECOND INPUT SERIES STARTS HERE!
	 */

	case R.id.login_screen_button_zero2:
	  intent.putExtra("ButtonID", 0);
	  break;

	case R.id.login_screen_button_one2:
	  intent.putExtra("ButtonID", 1);
	  break;

	case R.id.login_screen_button_two2:
	  intent.putExtra("ButtonID", 2);
	  break;

	case R.id.login_screen_button_three2:
	  intent.putExtra("ButtonID", 3);
	  break;

	case R.id.login_screen_button_four2:
	  intent.putExtra("ButtonID", 4);
	  break;

	case R.id.login_screen_button_five2:
	  intent.putExtra("ButtonID", 5);
	  break;

	case R.id.login_screen_button_six2:
	  intent.putExtra("ButtonID", 6);
	  break;

	case R.id.login_screen_button_seven2:
	  intent.putExtra("ButtonID", 7);
	  break;

	case R.id.login_screen_button_eight2:
	  intent.putExtra("ButtonID", 8);
	  break;

	case R.id.login_screen_button_nine2:
	  intent.putExtra("ButtonID", 9);
	  break;

	case R.id.login_screen_button_back2:
	  intent.putExtra("ButtonID", -1);
	  break;

	case R.id.login_screen_button_submit2:
	  intent.putExtra("ButtonID", -2);
	  break;

		/**
		 * THIRD INPUT SERIES STARTS HERE!
		 */

		case R.id.login_screen_button_zero3:
		  intent.putExtra("ButtonID", 0);
		  break;

		case R.id.login_screen_button_one3:
		  intent.putExtra("ButtonID", 1);
		  break;

		case R.id.login_screen_button_two3:
		  intent.putExtra("ButtonID", 2);
		  break;

		case R.id.login_screen_button_three3:
		  intent.putExtra("ButtonID", 3);
		  break;

		case R.id.login_screen_button_four3:
		  intent.putExtra("ButtonID", 4);
		  break;

		case R.id.login_screen_button_five3:
		  intent.putExtra("ButtonID", 5);
		  break;

		case R.id.login_screen_button_six3:
		  intent.putExtra("ButtonID", 6);
		  break;

		case R.id.login_screen_button_seven3:
		  intent.putExtra("ButtonID", 7);
		  break;

		case R.id.login_screen_button_eight3:
		  intent.putExtra("ButtonID", 8);
		  break;

		case R.id.login_screen_button_nine3:
		  intent.putExtra("ButtonID", 9);
		  break;

		case R.id.login_screen_button_back3:
		  intent.putExtra("ButtonID", -1);
		  break;

		case R.id.login_screen_button_submit3:
		  intent.putExtra("ButtonID", -2);
		  break;
	  
	default:
	  break;
	}

	context.sendBroadcast(intent);

  }

}
