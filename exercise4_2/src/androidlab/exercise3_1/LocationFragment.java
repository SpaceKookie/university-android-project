package androidlab.exercise3_1;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * This was supposed to become my DialogFragment to select a location. Well.
 * Time's up...
 * 
 * @author Konstantin
 * 
 */
public class LocationFragment extends DialogFragment {

    static LocationFragment newInstance() {
	LocationFragment frag = new LocationFragment();

	return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
	    Bundle savedInstanceState) {
	View view = inflater
		.inflate(R.layout.location_editor, container, false);
	View tv = view.findViewById(R.id.map_fragment_title);
	((TextView) tv).setText("This is text");

	return view;
    }

}
