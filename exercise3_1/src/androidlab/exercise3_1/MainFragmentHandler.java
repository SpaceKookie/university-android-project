package androidlab.exercise3_1;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Menu;

/**
 * This is the only activity of the App which will create the event_splash layout. Its just a frame-layout which will then be replaced by one
 * of my two fragments. It's really fast and efficient and makes the app seem smooth, even on a VM.
 * 
 * @author Konstantin
 * 
 */
public class MainFragmentHandler extends BaseActivity {

  Fragment getData = new GetDataFragment();
  Fragment setData = new SetDataFragment();
  private final String TAG = "MainFragmentHandler";

  @Override
  public boolean onCreateOptionsMenu(Menu m) {

	getMenuInflater().inflate(R.menu.activity_main, m);
	return true;
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {

	super.onCreate(savedInstanceState);
	setContentView(R.layout.event_splash);

	FragmentManager frmg = getSupportFragmentManager();
	FragmentTransaction frtr = frmg.beginTransaction();
	Log.i(TAG, "Created Fragment");
	frtr.replace(R.id.frag_frame, getData, "GET");
	frtr.addToBackStack(null);
	frtr.commit();
  }
}
