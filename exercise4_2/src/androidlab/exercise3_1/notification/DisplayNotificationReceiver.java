package androidlab.exercise3_1.notification;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import androidlab.exercise3_1.*;
import androidlab.exercise3_1.model.*;

/**
 * <h2>Handles the appearance of the remind notifications</h2> This
 * BroadcastReceiver will be called by the {@link AlarmManager} in the
 * {@link RemindService} class (see
 * {@link RemindService#createNotification(int, String, String, long)}).There
 * alarms are set to the date where a ToDo item has a remind date and it will
 * send at this time an intent to {@link DisplayNotificationReceiver}. These
 * intents are handled by the method
 * {@link DisplayNotificationReceiver#onReceive(Context, Intent)} which will
 * fetch the corresponding ToDo item from the database an create a
 * {@link Notification} for this item.
 * 
 * @see {@link BroadcastReceiver}, {@link RemindService}
 * @author Seminar 'Introduction to Android Smartphone Programming', University
 *         of Freiburg
 * @version 1.3
 **/
public class DisplayNotificationReceiver extends BroadcastReceiver {

	/**
	 * Called if this BraodcastReceiver receives the intent which informs about
	 * the occurrence of an remind date of a ToDo item. The received intent
	 * contains the identifier, the title and the description of the ToDo item
	 * as extra data. With these information the method will create a
	 * notification which will be published through the
	 * {@link NotificationManager} in the status bar.
	 * 
	 * @param context
	 *            The Context in which the receiver is running.
	 * @param intent
	 *            The Intent being received.
	 * 
	 * @see {@link BroadcastReceiver#onReceive(Context, Intent)},
	 *      {@link RemindService}, {@link Notification},
	 *      {@link NotificationManager}
	 **/
	@Override
	public void onReceive(Context context, Intent intent) {
		int id = intent.getIntExtra(ToDoItem.ID, -1);
		String title = intent.getStringExtra(ToDoItem.TITLE);
		String description = intent.getStringExtra(ToDoItem.DESCRIPTION);
		Intent target = new Intent(context, MainActivity.class);
		target.setAction(RemindService.REMIND_SERVICE);
		target.putExtra(ToDoItem.ID, id);
		target.setFlags(intent.getFlags() | Intent.FLAG_ACTIVITY_NEW_TASK);
		PendingIntent contentIntent = PendingIntent.getActivity(context, id,
				intent, PendingIntent.FLAG_UPDATE_CURRENT);
		Notification notification = new Notification.Builder(context)
				.setContentTitle(title).setContentText(description)
				.setContentIntent(contentIntent)
				.setSmallIcon(R.drawable.ic_launcher).build();
		NotificationManager notificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.notify(id, notification);
	}

}
