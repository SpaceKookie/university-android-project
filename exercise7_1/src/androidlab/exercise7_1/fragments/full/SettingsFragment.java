package androidlab.exercise7_1.fragments.full;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.PreferenceFragment;
import androidlab.exercise7_1.R;
import androidlab.exercise7_1.core.AlarmService;
import androidlab.exercise7_1.utility.AppSettingsHelper;

/* 
 * Copyright (c) 2013 Katharina Sabel
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Much like in the last assignment I build a little Settings screen for a user to play around with special setups.
 * 
 * @author Katharina
 * 
 */
public class SettingsFragment extends PreferenceFragment implements OnSharedPreferenceChangeListener {

  private CheckBoxPreference application;
  private CheckBoxPreference alarm;
  private CheckBoxPreference silent;
  private CheckBoxPreference lockdown;
  private Context context;

  @Override
  public void onCreate(Bundle savedInstanceState) {

	super.onCreate(savedInstanceState);
	addPreferencesFromResource(R.layout.layout_settings_fragment);
	getPreferenceManager().setSharedPreferencesName(AppSettingsHelper.PREFERENCES_TABLE_NAME);
	getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
	context = this.getActivity();

	// Initializing the CheckBoxPreferences from @BASIC SETUP
	application = (CheckBoxPreference) getPreferenceScreen().findPreference(
		AppSettingsHelper.APPLICATION_ENABLE_KEY);
	alarm = (CheckBoxPreference) getPreferenceScreen().findPreference(AppSettingsHelper.LOUD_ALARM_KEY);
	silent = (CheckBoxPreference) getPreferenceScreen().findPreference(AppSettingsHelper.SILENT_ALARM_KEY);
	lockdown = (CheckBoxPreference) getPreferenceScreen().findPreference(AppSettingsHelper.LOCKDOWN_KEY);

	// Will set the CheckBoxes according to the saved Preferences
	application.setChecked(AppSettingsHelper.getApplicationState(context));
	alarm.setChecked(AppSettingsHelper.getLoudAlarmFlag(context));
	silent.setChecked(AppSettingsHelper.getSilentAlarmFlag(context));
	lockdown.setChecked(AppSettingsHelper.getLockdownFlag(context));

  }

  @Override
  public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

	if (key.equals(AppSettingsHelper.APPLICATION_ENABLE_KEY)) {

	  AppSettingsHelper.setApplicationState(
		  sharedPreferences.getBoolean(AppSettingsHelper.APPLICATION_ENABLE_KEY, true), context);

	  if (!AppSettingsHelper.getApplicationState(context)) {
		alarm.setEnabled(false);
		silent.setEnabled(false);
		lockdown.setEnabled(false);
	  }
	  else {
		alarm.setEnabled(true);
		silent.setEnabled(true);
		lockdown.setEnabled(true);
	  }
	}
	else if (key.equals(AppSettingsHelper.LOUD_ALARM_KEY)) {
	  AppSettingsHelper.setLoudAlarmFlag(
		  sharedPreferences.getBoolean(AppSettingsHelper.LOUD_ALARM_KEY, true), context);

	  if (AppSettingsHelper.getApplicationState(context) && AppSettingsHelper.getLoudAlarmFlag(context)) {
		System.out.println("I really want to start the service");
		Intent intent = new Intent(context, AlarmService.class);
		context.startService(intent);
	  }

	}
	else if (key.equals(AppSettingsHelper.SILENT_ALARM_KEY)) {
	  AppSettingsHelper.setSilentAlarmFlag(
		  sharedPreferences.getBoolean(AppSettingsHelper.SILENT_ALARM_KEY, true), context);
	}
	else if (key.equals(AppSettingsHelper.LOCKDOWN_KEY)) {
	  AppSettingsHelper.setLockdownFlag(sharedPreferences.getBoolean(AppSettingsHelper.LOCKDOWN_KEY, true),
		  context);
	}

  }

  @Override
  public void onResume() {
	super.onResume();
	getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);

  }

  @Override
  public void onPause() {
	super.onPause();
	getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
  }

}
