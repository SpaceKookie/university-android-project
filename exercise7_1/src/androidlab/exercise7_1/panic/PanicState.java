package androidlab.exercise7_1.panic;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;
import androidlab.exercise7_1.core.PanicActivity;
import androidlab.exercise7_1.utility.AppSettingsHelper;

/* 
 * Copyright (c) 2013 Katharina Sabel
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Initial State to be called when panic hits. Will call @PanicFragment and @CustomPanicService to make panic. This is a
 * stupid comment :)
 * 
 * @author Katharina
 * 
 */
public class PanicState {

	AudioManager sound;
	Ringtone tone;
	Context context;

	public PanicState(Context context) {
		IntentFilter filter = new IntentFilter(AppSettingsHelper.ALARM_INTENT_FILTER);

		this.context = context;
		context.registerReceiver(receiver, filter);
	}

	public PanicState(Context contenxt, int n) {
		if (n == 0)
			context.unregisterReceiver(receiver);
	}

	BroadcastReceiver receiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {

			// Checks if the intent came out alright
			if (intent.hasExtra(AppSettingsHelper.TRIGGER_ALARM)) {
				Log.i("Alarm", "Extra was cought");

				// Will be called if the boolean extra is @true
				if (intent.getBooleanExtra(AppSettingsHelper.TRIGGER_ALARM, false)) {

					soYouWantToCryOutLoud();

				}

				// Will be called if the boolean extra is @false
				else {

					soYouWantToShutUpAgain();

				}

			} else {
				Log.i("Alarm Intent", "The intent came out all wrong");
			}

			if (AppSettingsHelper.getPanicFlag(context)) {
				tone.stop();
				Toast.makeText(context, "I am fine again", Toast.LENGTH_SHORT).show();
				AppSettingsHelper.setPanicFlag(false, context);
			}
		}
	};

	/**
	 * Creating to seperate methods for activating and deactivating the alarm to uncluster the code. Stopping the alarm
	 */
	private void soYouWantToShutUpAgain() {

		if (AppSettingsHelper.getPanicFlag(context)) {
			tone.stop();
			AppSettingsHelper.setPanicFlag(false, context);
		}

	}

	/**
	 * Starting the alarm
	 */
	private void soYouWantToCryOutLoud() {
		AppSettingsHelper.setPanicFlag(true, context);

		// TODO: Song picker or something
		Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
		tone = RingtoneManager.getRingtone(context, notification);

		if (!tone.isPlaying()) {

			tone.play();
		}

		context.startActivity(new Intent(context, PanicActivity.class));

	}
}