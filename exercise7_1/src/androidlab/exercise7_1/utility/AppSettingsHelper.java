package androidlab.exercise7_1.utility;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import androidlab.exercise7_1.R;

/* 
 * Copyright (c) 2013 Katharina Sabel
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * After this class was really handy in the last exercise I decided to do the exact same thing again. This class handles
 * all input and output to the SharedPrefences database. As well as static @String objects that parts of the application
 * can use such as @IntentFilter strings.
 * 
 * @author Katharina
 * 
 */
public class AppSettingsHelper {

  private static SharedPreferences prefs;
  public static String TRIGGER_ALARM;
  public static String NUMPAD_INTENT_FILTER;
  public static String ALARM_INTENT_FILTER;
  public static String PREFERENCES_TABLE_NAME;
  public static String PREFERENCES_KEY_PIN;
  public static String PREFERENCES_KEY_FIRST;
  public static String APPLICATION_ENABLE_KEY;
  public static String LOUD_ALARM_KEY;
  public static String SILENT_ALARM_KEY;
  public static String LOCKDOWN_KEY;
  public static String PANIC_STATE_KEY;
  public static String PANIC_INTENT_FILTER;

  public AppSettingsHelper(Context context) {
	NUMPAD_INTENT_FILTER = context.getString(R.string.NUMPAD_INTENT_FILTER);
	ALARM_INTENT_FILTER = context.getString(R.string.ALARM_INTENT_FILTER);
	PREFERENCES_KEY_PIN = context.getString(R.string.PREFERENCES_KEY_PIN);
	PREFERENCES_KEY_FIRST = context.getString(R.string.PREFERENCES_KEY_FIRST);
	APPLICATION_ENABLE_KEY = context.getString(R.string.APPLICATION_ENABLE_KEY);
	LOUD_ALARM_KEY = context.getString(R.string.LOUD_ALARM_KEY);
	SILENT_ALARM_KEY = context.getString(R.string.SILENT_ALARM_KEY);
	LOCKDOWN_KEY = context.getString(R.string.LOCKDOWN_KEY);
	TRIGGER_ALARM = context.getString(R.string.ALARM_INTENT);
	PANIC_INTENT_FILTER = context.getString(R.string.PANIC_INTENT_BUTTONS);
  }

  public static void setApplicationPin(String pin, Context context) {
	prefs = context.getSharedPreferences(PREFERENCES_TABLE_NAME, 0);
	Editor pe = prefs.edit();
	pe.putString(PREFERENCES_KEY_PIN, pin);
	pe.commit();
  }

  public static String getApplicationPin(Context context) {
	prefs = context.getSharedPreferences(PREFERENCES_TABLE_NAME, 0);
	return prefs.getString(PREFERENCES_KEY_PIN, null);
  }

  public static void setFirstTimeFlag(boolean flag, Context context) {
	prefs = context.getSharedPreferences(PREFERENCES_TABLE_NAME, 0);
	Editor pe = prefs.edit();
	pe.putBoolean(PREFERENCES_KEY_FIRST, flag);
	pe.commit();
  }

  public static boolean getFirstTimeFlag(Context context) {
	prefs = context.getSharedPreferences(PREFERENCES_TABLE_NAME, 0);
	return prefs.getBoolean(PREFERENCES_KEY_FIRST, true);
  }

  public static void setApplicationState(boolean flag, Context context) {
	prefs = context.getSharedPreferences(PREFERENCES_TABLE_NAME, 0);
	Editor pe = prefs.edit();
	pe.putBoolean(APPLICATION_ENABLE_KEY, flag);
	pe.commit();
  }

  public static boolean getApplicationState(Context context) {
	prefs = context.getSharedPreferences(PREFERENCES_TABLE_NAME, 0);
	return prefs.getBoolean(APPLICATION_ENABLE_KEY, false);
  }

  public static void setLoudAlarmFlag(boolean flag, Context context) {
	prefs = context.getSharedPreferences(PREFERENCES_TABLE_NAME, 0);
	Editor pe = prefs.edit();
	pe.putBoolean(LOUD_ALARM_KEY, flag);
	pe.commit();
  }

  public static boolean getLoudAlarmFlag(Context context) {
	prefs = context.getSharedPreferences(PREFERENCES_TABLE_NAME, 0);
	return prefs.getBoolean(LOUD_ALARM_KEY, false);
  }

  public static void setSilentAlarmFlag(boolean flag, Context context) {
	prefs = context.getSharedPreferences(PREFERENCES_TABLE_NAME, 0);
	Editor pe = prefs.edit();
	pe.putBoolean(SILENT_ALARM_KEY, flag);
	pe.commit();
  }

  public static boolean getSilentAlarmFlag(Context context) {
	prefs = context.getSharedPreferences(PREFERENCES_TABLE_NAME, 0);
	return prefs.getBoolean(SILENT_ALARM_KEY, false);
  }

  public static void setLockdownFlag(boolean flag, Context context) {
	prefs = context.getSharedPreferences(PREFERENCES_TABLE_NAME, 0);
	Editor pe = prefs.edit();
	pe.putBoolean(LOCKDOWN_KEY, flag);
	pe.commit();
  }

  public static boolean getLockdownFlag(Context context) {
	prefs = context.getSharedPreferences(PREFERENCES_TABLE_NAME, 0);
	return prefs.getBoolean(LOCKDOWN_KEY, false);
  }

  public static void setPanicFlag(boolean flag, Context context) {
	prefs = context.getSharedPreferences(PREFERENCES_TABLE_NAME, 0);
	Editor pe = prefs.edit();
	pe.putBoolean(PANIC_STATE_KEY, flag);
	pe.commit();
  }

  public static boolean getPanicFlag(Context context) {
	prefs = context.getSharedPreferences(PREFERENCES_TABLE_NAME, 0);
	return prefs.getBoolean(PANIC_STATE_KEY, false);
  }

}
