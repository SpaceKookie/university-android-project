package androidlab.exercise5_1;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLUtils;
import android.util.Log;
import androidlab.exercise5_1.R;

public class CubicPolygon {

  private FloatBuffer vertexBuffer;
  private FloatBuffer textureBuffer;
  private ByteBuffer indexBuffer;
  private Bitmap bmp1, bmp2, bmp3, bmp4, bmp5, bmp6;

  private float vertices[] = {
	  // Vertices according to faces
	  -1.0f, -1.0f, 1.0f, // Vertex 0
	  1.0f, -1.0f, 1.0f,  // v1
	  -1.0f, 1.0f, 1.0f,  // v2
	  1.0f, 1.0f, 1.0f,   // v3

	  1.0f, -1.0f, 1.0f,	// ...
	  1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, -1.0f,

	  1.0f, -1.0f, -1.0f, -1.0f, -1.0f, -1.0f, 1.0f, 1.0f, -1.0f, -1.0f, 1.0f, -1.0f,

	  -1.0f, -1.0f, -1.0f, -1.0f, -1.0f, 1.0f, -1.0f, 1.0f, -1.0f, -1.0f, 1.0f, 1.0f,

	  -1.0f, -1.0f, -1.0f, 1.0f, -1.0f, -1.0f, -1.0f, -1.0f, 1.0f, 1.0f, -1.0f, 1.0f,

	  -1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, -1.0f, 1.0f, -1.0f, 1.0f, 1.0f, -1.0f, };

  /** The initial texture coordinates (u, v) */
  private float texture[] = {
	  // Mapping coordinates for the vertices
	  0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f,

	  0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f,

	  0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f,

	  0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f,

	  0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f,

	  0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f,

  };

  /** The initial indices definition */
  private byte indices[] = {
	  // Faces definition
	  0, 1, 3, 0, 3, 2, 			// Face front
	  4, 5, 7, 4, 7, 6, 			// Face right
	  8, 9, 11, 8, 11, 10, 		// ...
	  12, 13, 15, 12, 15, 14, 16, 17, 19, 16, 19, 18, 20, 21, 23, 20, 23, 22, };

  /**
   * The Cube constructor.
   * 
   * Initiate the buffers.
   */
  public CubicPolygon() {

	ByteBuffer byteBuf = ByteBuffer.allocateDirect(vertices.length * 4);
	byteBuf.order(ByteOrder.nativeOrder());
	vertexBuffer = byteBuf.asFloatBuffer();
	vertexBuffer.put(vertices);
	vertexBuffer.position(0);

	byteBuf = ByteBuffer.allocateDirect(texture.length * 4);
	byteBuf.order(ByteOrder.nativeOrder());
	textureBuffer = byteBuf.asFloatBuffer();
	textureBuffer.put(texture);
	textureBuffer.position(0);

	indexBuffer = ByteBuffer.allocateDirect(indices.length);
	indexBuffer.put(indices);
	indexBuffer.position(0);
  }

  /**
   * The object own drawing function. Called from the renderer to redraw this instance with possible changes in values.
   * 
   * @param gl
   *          - The GL Context
   */
  public void draw(GL10 gl) {

	// Bind our only previously generated textures in this case

	// // Point to our buffers
	gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
	gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
	//
	// // Set the face rotation
	gl.glFrontFace(GL10.GL_CCW);
	//
	// // Enable the vertex and texture state
	gl.glVertexPointer(3, GL10.GL_FLOAT, 0, vertexBuffer);
	gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, textureBuffer);
	//
	// // Draw the vertices as triangles, based on the Index Buffer information
	gl.glDrawElements(GL10.GL_TRIANGLES, indices.length, GL10.GL_UNSIGNED_BYTE, indexBuffer);
	makeDice(gl);
	//
	// // Disable the client state before leaving
	gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
	gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
  }

  /**
   * 
   * Texturizes the dice (taken from the Cube Object provided to us)
   * 
   * @param gl
   */
  private void makeDice(GL10 gl) {

	gl.glColor4f(1, 1, 1, 1);
	gl.glNormal3f(0, 0, 1);
	gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 0, 4);
	GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, bmp1, 0);
	gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_LINEAR);
	gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR);

	gl.glColor4f(1, 1, 1, 1);
	gl.glNormal3f(0, 0, -1);
	gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 4, 4);
	GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, bmp2, 0);
	gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_LINEAR);
	gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR);

	gl.glColor4f(1, 1, 1, 1);
	gl.glNormal3f(-1, 0, 0);
	gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 8, 4);
	GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, bmp3, 0);
	gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_LINEAR);
	gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR);

	gl.glColor4f(1, 1, 1, 1);
	gl.glNormal3f(1, 0, 0);
	gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 12, 4);
	GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, bmp4, 0);
	gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_LINEAR);
	gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR);

	gl.glColor4f(1, 1, 1, 1);
	gl.glNormal3f(0, 1, 0);
	gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 16, 4);
	GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, bmp5, 0);
	gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_LINEAR);
	gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR);

	gl.glColor4f(1, 1, 1, 1);
	gl.glNormal3f(0, -1, 0);
	gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 20, 4);
	GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, bmp6, 0);
	gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_LINEAR);
	gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR);

  }

  /**
   * Load the textures
   * 
   * @param gl
   *          - The GL Context
   * @param context
   *          - The Activity context
   */
  public void loadGLTexture(GL10 gl, Context context) {

	// Get the texture from the Android resource directory
	InputStream one = context.getResources().openRawResource(R.drawable.wuerfel01_512);
	InputStream two = context.getResources().openRawResource(R.drawable.wuerfel02_512);
	InputStream three = context.getResources().openRawResource(R.drawable.wuerfel03_512);
	InputStream four = context.getResources().openRawResource(R.drawable.wuerfel04_512);
	InputStream five = context.getResources().openRawResource(R.drawable.wuerfel05_512);
	InputStream six = context.getResources().openRawResource(R.drawable.wuerfel06_512);

	try {

	  bmp1 = BitmapFactory.decodeStream(one);
	  bmp2 = BitmapFactory.decodeStream(two);
	  bmp3 = BitmapFactory.decodeStream(three);
	  bmp4 = BitmapFactory.decodeStream(four);
	  bmp5 = BitmapFactory.decodeStream(five);
	  bmp6 = BitmapFactory.decodeStream(six);

	}
	finally {
	  // Always clear and close
	  try {
		one.close();
		two.close();
		three.close();
		four.close();
		five.close();
		six.close();

		one = null;
		two = null;
		three = null;
		four = null;
		five = null;
		six = null;
	  }
	  catch (IOException e) {
		Log.i("ERROR", "Textures couldn't be loaded!");
	  }
	}
  }
}
